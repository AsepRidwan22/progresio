import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:frontmobile_blog_app/data/helper/secure_storage_helper.dart';
import 'package:frontmobile_blog_app/domain/repositories/user_repository.dart';
import 'package:mockito/annotations.dart';
import 'package:http/http.dart' as http;

@GenerateMocks([
  UserRepository,
  FlutterSecureStorage,
  SecureStorageHelper,
  http.Client, // Menambahkan http.Client sebagai bagian dari mock
], customMocks: [
  MockSpec<http.Client>(as: #MockHttpClient),
])
void main() {}
