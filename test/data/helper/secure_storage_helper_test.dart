import 'package:frontmobile_blog_app/data/helper/secure_storage_helper.dart';
import 'package:mockito/mockito.dart';
import 'package:test/test.dart';
import '../../helpers/test_helper.mocks.dart';

void main() {
  late SecureStorageHelper secureStorageHelper;
  late MockFlutterSecureStorage mockSecureStorage;

  setUp(() {
    mockSecureStorage = MockFlutterSecureStorage();
    secureStorageHelper = SecureStorageHelper(mockSecureStorage);
  });

  group('Secure Storage Helper', () {
    test('should save and retrieve token successfully', () async {
      // Arrange
      when(mockSecureStorage.write(
        key: SecureStorageHelper.tokenKey,
        value: 'test_token',
      )).thenAnswer((_) async {});

      when(mockSecureStorage.read(key: SecureStorageHelper.tokenKey))
          .thenAnswer((_) async => 'test_token');

      // Act
      await secureStorageHelper.saveToken('test_token');
      final result = await secureStorageHelper.getToken();

      // Assert
      expect(result, 'test_token');
    });

    test('should return token when calling getToken', () async {
      // Arrange
      const expectedToken = 'test_token';
      when(mockSecureStorage.read(key: SecureStorageHelper.tokenKey))
          .thenAnswer((_) async => expectedToken);

      // Act
      final result = await secureStorageHelper.getToken();

      // Assert
      expect(result, equals(expectedToken));
      verify(mockSecureStorage.read(key: SecureStorageHelper.tokenKey));
      verifyNoMoreInteractions(mockSecureStorage);
    });

    test('should return null when calling getToken and no token is stored',
        () async {
      // Arrange
      when(mockSecureStorage.read(key: SecureStorageHelper.tokenKey))
          .thenAnswer((_) async => null);

      // Act
      final result = await secureStorageHelper.getToken();

      // Assert
      expect(result, isNull);
      verify(mockSecureStorage.read(key: SecureStorageHelper.tokenKey));
      verifyNoMoreInteractions(mockSecureStorage);
    });
  });
}
