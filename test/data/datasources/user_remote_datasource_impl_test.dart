import 'dart:convert';
import 'package:flutter_test/flutter_test.dart';
// import 'package:frontmobile_blog_app/common/config.dart';
import 'package:frontmobile_blog_app/common/failure.dart';
import 'package:frontmobile_blog_app/data/datasources/user_remote_datasource_impl.dart';
import 'package:frontmobile_blog_app/data/model/requests/request_registration_model.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_login_model.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_register_model.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/mockito.dart';

import '../../helpers/test_helper.mocks.dart';

void main() {
  late UserRemoteDataSource dataSource;
  late MockHttpClient mockHttpClient;
  late MockSecureStorageHelper mockSecureStorageHelper;

  setUp(() {
    mockHttpClient = MockHttpClient();
    mockSecureStorageHelper = MockSecureStorageHelper();
    dataSource = UserRemoteDataSourceImpl(
      client: mockHttpClient,
      secureStorageHelper: mockSecureStorageHelper,
    );
  });

  group('login', () {
    test('should return LoginModel when the call to API is successful',
        () async {
      // Arrange
      const mockResponse =
          '{"success": true, "message": "", "access_token": "mock_token"}';
      const expectedModel =
          ResponseLoginModel(success: true, user: null, message: '');
      when(mockHttpClient.post(any,
              body: anyNamed('body'), headers: anyNamed('headers')))
          .thenAnswer((_) async => http.Response(mockResponse, 200));

      // Act
      final result = await dataSource.login('test@example.com', 'password');

      // Assert
      expect(result, equals(expectedModel));
      verify(mockSecureStorageHelper.saveToken('mock_token')).called(1);
    });

    test('should throw ServerFailure when the call to API is unsuccessful',
        () async {
      // Arrange
      const mockErrorResponse = '{"success": false, "message": "Login failed"}';
      when(mockHttpClient.post(any,
              body: anyNamed('body'), headers: anyNamed('headers')))
          .thenAnswer((_) async => http.Response(mockErrorResponse, 401));

      // Act & Assert
      expect(() async => await dataSource.login('test@example.com', 'password'),
          throwsA(isA<ServerFailure>()));
      verifyZeroInteractions(mockSecureStorageHelper);
    });
  });

  group('register', () {
    test('should return RegisterModel when registration is successful',
        () async {
      // Arrange
      const mockResponse =
          '{"success": true, "message": "Registration successful"}';
      when(mockHttpClient.post(any,
              body: anyNamed('body'), headers: anyNamed('headers')))
          .thenAnswer((_) async => http.Response(mockResponse, 200));

      // Act
      final result = await dataSource.register(const RequestRegistrationModel(
        name: 'John Doe',
        email: 'john.doe@example.com',
        password: 'password',
      ));

      // Assert
      expect(result, isA<RegisterModel>());
    });

    test('should throw ServerFailure when the call to API is unsuccessful',
        () async {
      // Arrange
      when(mockHttpClient.post(any,
              body: anyNamed('body'), headers: anyNamed('headers')))
          .thenAnswer((_) async => http.Response(
              jsonEncode({'message': 'Email already exists'}), 401));

      // Act & Assert
      expect(
        () async => await dataSource.register(const RequestRegistrationModel(
          name: 'udin saparudin',
          email: 'udinsaparudin2222@gmail.com',
          password: 'password',
        )),
        throwsA(isA<ServerFailure>()),
      );
    });
  });

  // group('logout', () {
  //   test(
  //       'should return success message and delete token when the call to API is successful',
  //       () async {
  //     // Arrange
  //     const mockResponse = '{"success": true, "message": "Logout successful"}';
  //     when(mockHttpClient.post(any, headers: anyNamed('headers')))
  //         .thenAnswer((_) async => http.Response(mockResponse, 200));

  //     // Act
  //     final result = await dataSource.logout();

  //     // Assert
  //     expect(result, equals('Logout successful'));
  //     verify(mockSecureStorageHelper.deleteToken()).called(1);
  //   });

  //   test('should throw ServerFailure when the call to API is unsuccessful',
  //       () async {
  //     // Arrange
  //     const errorMessage = 'Unauthorized';

  //     when(mockHttpClient.post(
  //       any,
  //       headers: anyNamed('headers'),
  //     )).thenAnswer((_) async =>
  //         http.Response(jsonEncode({'message': errorMessage}), 401));

  //     // Act & Assert
  //     expect(
  //         () async => await dataSource.logout(), throwsA(isA<ServerFailure>()));

  //     verify(mockHttpClient.post(
  //       Uri.parse('${Config().baseURL}/api/logout'),
  //       headers: {'Content-Type': 'application/json'},
  //     ));

  //     verifyNever(mockSecureStorageHelper.deleteToken());
  //   });
  // });
}
