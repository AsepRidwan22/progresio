import 'package:flutter_test/flutter_test.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_login_model.dart';
import 'package:frontmobile_blog_app/domain/entities/user_entity.dart';
import '../../../dummy_data/dummy_objects.dart';

void main() {
  group('Login Model', () {
    // Test case for creating LoginModel instance
    test('should create LoginModel instance', () {
      // Arrange
      // make object

      // Assert
      expect(testLoginModel.success, true);
      expect(testLoginModel.message, 'Login successful');
      expect(testLoginModel.user, isA<UserEntity>());
    });

    // Test case for creating LoginModel instance with null user
    test('should create LoginModel instance with null user', () {
      // Arrange
      const loginModel = ResponseLoginModel(
        success: true,
        message: 'Login successful',
        user: null,
      );

      // Assert
      expect(loginModel.success, true);
      expect(loginModel.message, 'Login successful');
      expect(loginModel.user, isNull);
    });

    // Test case for creating LoginModel instance from JSON
    test('should create LoginModel instance from JSON', () {
      // Arrange
      final json = {
        'success': true,
        'message': 'Login successful',
        'user': {
          'id': '1',
          'name': 'John Doe',
          'nik': '1234567890123456',
          'phone_number': '083107113661',
          'email': 'john@example.com',
          'role': 'user',
        },
      };

      // Act
      final loginModel = ResponseLoginModel.fromJson(json);

      // Assert
      expect(loginModel.success, true);
      expect(loginModel.message, 'Login successful');
      expect(loginModel.user, isA<UserEntity>());
    });

    // Test case for creating LoginModel instance from JSON with null user
    test('should create LoginModel instance from JSON with null user', () {
      // Arrange
      final json = {
        'success': true,
        'message': 'Login successful',
        'user': null,
      };

      // Act
      final loginModel = ResponseLoginModel.fromJson(json);

      // Assert
      expect(loginModel.success, true);
      expect(loginModel.message, 'Login successful');
      expect(loginModel.user, isNull);
    });

    // Test case for creating LoginModel instance from JSON with null all
    test('should create LoginModel instance from JSON with null all', () {
      // Arrange
      final json = {
        'success': null,
        'message': null,
        'user': null,
      };

      // Act
      final loginModel = ResponseLoginModel.fromJson(json);

      // Assert
      expect(loginModel.success, false);
      expect(loginModel.message, 'Information not available.');
      expect(loginModel.user, isNull);
    });
  });
}
