import 'package:flutter_test/flutter_test.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_register_model.dart';

void main() {
  group('Register Model', () {
    // Test case for creating RegisterModel instance
    test('should create RegisterModel instance', () {
      // Arrange
      const registerModel =
          RegisterModel(success: true, message: 'Registration successful');

      // Assert
      expect(registerModel.success, true);
      expect(registerModel.message, 'Registration successful');
    });

    // Test case for creating RegisterModel instance from JSON
    test('should create RegisterModel instance from JSON', () {
      // Arrange
      final json = {'success': true, 'message': 'Registration successful'};

      // Act
      final registerModel = RegisterModel.fromJson(json);

      // Assert
      expect(registerModel.success, true);
      expect(registerModel.message, 'Registration successful');
    });

    // Test case for creating RegisterModel instance from JSON with null values
    test('should create RegisterModel instance from JSON with null values', () {
      // Arrange
      final json = {'success': null, 'message': null};

      // Act
      final registerModel = RegisterModel.fromJson(json);

      // Assert
      expect(registerModel.success, isFalse);
      expect(registerModel.message, '');
    });
  });
}
