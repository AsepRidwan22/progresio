import 'package:flutter_test/flutter_test.dart';
import 'package:frontmobile_blog_app/data/model/requests/request_registration_model.dart';

void main() {
  group('RequestRegistrationModel', () {
    // Test case for creating RequestRegistrationModel instance
    test('should create RequestRegistrationModel instance', () {
      // Arrange
      const requestRegistrationModel = RequestRegistrationModel(
        name: 'John Doe',
        email: 'john.doe@example.com',
        password: 'password123',
      );

      // Assert
      expect(requestRegistrationModel.name, 'John Doe');
      expect(requestRegistrationModel.email, 'john.doe@example.com');
      expect(requestRegistrationModel.password, 'password123');
    });

    // Test case for creating RequestRegistrationModel instance from JSON
    test('should create RequestRegistrationModel instance from JSON', () {
      // Arrange
      final json = {
        'name': 'John Doe',
        'email': 'john.doe@example.com',
        'password': 'password123',
      };

      // Act
      final requestRegistrationModel = RequestRegistrationModel.fromJson(json);

      // Assert
      expect(requestRegistrationModel.name, 'John Doe');
      expect(requestRegistrationModel.email, 'john.doe@example.com');
      expect(requestRegistrationModel.password, 'password123');
    });

    // Test case for creating JSON from RequestRegistrationModel
    test('should create JSON from RequestRegistrationModel', () {
      // Arrange
      const requestRegistrationModel = RequestRegistrationModel(
        name: 'John Doe',
        email: 'john.doe@example.com',
        password: 'password123',
      );

      // Act
      final json = requestRegistrationModel.toJson();

      // Assert
      expect(json['name'], 'John Doe');
      expect(json['email'], 'john.doe@example.com');
      expect(json['password'], 'password123');
    });

    // Test case for Equality Testing
    test('instances with the same values should be equal', () {
      // Arrange
      const model1 = RequestRegistrationModel(
        name: 'John Doe',
        email: 'john.doe@example.com',
        password: 'password123',
      );
      const model2 = RequestRegistrationModel(
        name: 'John Doe',
        email: 'john.doe@example.com',
        password: 'password123',
      );

      // Assert
      expect(model1, equals(model2));
    });

    // Test case for Nullability
    test('should handle null values', () {
      // Arrange
      final json = {
        'name': 'John Doe',
        'email': null,
        'password': 'password123',
      };

      // Act
      final requestRegistrationModel = RequestRegistrationModel.fromJson(json);

      // Assert
      expect(requestRegistrationModel.name, 'John Doe');
      expect(requestRegistrationModel.email, isNull);
      expect(requestRegistrationModel.password, 'password123');
    });
  });
}
