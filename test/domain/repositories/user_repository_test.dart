import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:frontmobile_blog_app/data/model/requests/request_registration_model.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_register_model.dart';
import 'package:mockito/mockito.dart';
import 'package:frontmobile_blog_app/common/failure.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_login_model.dart';
import 'package:frontmobile_blog_app/domain/repositories/user_repository.dart';
import '../../helpers/test_helper.mocks.dart';

void main() {
  late MockUserRepository mockUserRepository;
  late UserRepository userRepository;

  setUp(() {
    mockUserRepository = MockUserRepository();
    userRepository = mockUserRepository;
  });

  group('User Repository', () {
    // Test case for registration
    test('should register successfully', () async {
      // Arrange
      const response =
          RegisterModel(success: true, message: 'Registration successful');
      when(mockUserRepository.register(const RequestRegistrationModel(
              name: 'udin saparudin',
              email: 'udinsaparudin2222@gmail.com',
              password: 'password')))
          .thenAnswer((_) async => const Right(response));

      // Act
      final result = await userRepository.register(
          const RequestRegistrationModel(
              name: 'udin saparudin',
              email: 'udinsaparudin2222@gmail.com',
              password: 'password'));

      // Assert
      expect(result, const Right(response));
      verify(mockUserRepository.register(const RequestRegistrationModel(
          name: 'udin saparudin',
          email: 'udinsaparudin2222@gmail.com',
          password: 'password')));
      verifyNoMoreInteractions(mockUserRepository);
    });

    // Test case for registration failure
    test('should fail to register', () async {
      // Arrange
      when(mockUserRepository.register(const RequestRegistrationModel(
              name: 'udin saparudin',
              email: 'udinsaparudin2222@gmail.com',
              password: 'password')))
          .thenAnswer(
              (_) async => const Left(ServerFailure('Registration failed')));

      // Act
      final result = await userRepository.register(
          const RequestRegistrationModel(
              name: 'udin saparudin',
              email: 'udinsaparudin2222@gmail.com',
              password: 'password'));

      // Assert
      expect(result, const Left(ServerFailure('Registration failed')));
      verify(mockUserRepository.register(const RequestRegistrationModel(
          name: 'udin saparudin',
          email: 'udinsaparudin2222@gmail.com',
          password: 'password')));
      verifyNoMoreInteractions(mockUserRepository);
    });

    // Test case for login
    test('should login successfully', () async {
      // Arrange
      when(mockUserRepository.login('john@example.com', 'password'))
          .thenAnswer((_) async => const Right(ResponseLoginModel(
                success: true,
                message: 'Login successful',
              )));

      // Act
      final result = await userRepository.login('john@example.com', 'password');

      // Assert
      expect(
        result,
        const Right(
            ResponseLoginModel(success: true, message: 'Login successful')),
      );
      verify(mockUserRepository.login('john@example.com', 'password'));
      verifyNoMoreInteractions(mockUserRepository);
    });

    // Test case for login failure
    test('should fail to login', () async {
      // Arrange
      when(mockUserRepository.login('john@example.com', 'password')).thenAnswer(
          (_) async => const Left(ServerFailure('Invalid credentials')));

      // Act
      final result = await userRepository.login('john@example.com', 'password');

      // Assert
      expect(result, const Left(ServerFailure('Invalid credentials')));
      verify(mockUserRepository.login('john@example.com', 'password'));
      verifyNoMoreInteractions(mockUserRepository);
    });
  });
}
