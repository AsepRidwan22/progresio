import 'package:flutter_test/flutter_test.dart';
import 'package:frontmobile_blog_app/domain/entities/user_entity.dart';

void main() {
  group('UserEntity', () {
    test('should create UserEntity from JSON', () {
      // Arrange
      final json = {
        'id': '1',
        'name': 'John Doe',
        'email': 'john@example.com',
        'nik': '1234567890123456',
        'phone_number': '083107113661',
        'role': 'admin',
        'created_at': '2023-01-01T12:00:00Z',
        'updated_at': '2023-01-02T12:30:00Z',
      };

      // Act
      final userEntity = UserEntity.fromJson(json);

      // Assert
      expect(userEntity.id, '1');
      expect(userEntity.name, 'John Doe');
      expect(userEntity.email, 'john@example.com');
      expect(userEntity.role, 'admin');
      expect(userEntity.createdAt, DateTime.utc(2023, 1, 1, 12, 0, 0));
      expect(userEntity.updatedAt, DateTime.utc(2023, 1, 2, 12, 30, 0));
    });

    test('should return correct props', () {
      // Arrange
      final userEntity1 = UserEntity(
        id: '1',
        name: 'John Doe',
        nik: '1234567890123456',
        noHp: '083107113661',
        email: 'john@example.com',
        role: 'admin',
        createdAt: DateTime.utc(2023, 1, 1, 12, 0, 0),
        updatedAt: DateTime.utc(2023, 1, 2, 12, 30, 0),
      );

      final userEntity2 = UserEntity(
        id: '1',
        name: 'John Doe',
        nik: '1234567890123456',
        noHp: '083107113661',
        email: 'john@example.com',
        role: 'admin',
        createdAt: DateTime.utc(2023, 1, 1, 12, 0, 0),
        updatedAt: DateTime.utc(2023, 1, 2, 12, 30, 0),
      );

      // Act
      final props1 = userEntity1.props;
      final props2 = userEntity2.props;

      // Assert
      expect(props1, props2);
    });
  });
}
