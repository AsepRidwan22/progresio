import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:frontmobile_blog_app/common/failure.dart';
import 'package:frontmobile_blog_app/domain/usecases/login_usecase.dart';
import '../../dummy_data/dummy_objects.dart';
import '../../helpers/test_helper.mocks.dart';

// class MockUserRepository extends Mock implements UserRepository {}

void main() {
  late MockUserRepository mockUserRepository;
  late LoginUsecase loginUsecase;

  setUp(() {
    mockUserRepository = MockUserRepository();
    loginUsecase = LoginUsecase(mockUserRepository);
  });

  test('should return LoginModel from the repository', () async {
    // Arrange
    when(mockUserRepository.login(testUser.email, testUser.password!))
        .thenAnswer((_) async => Right(testLoginModel));

    // Act
    final result =
        await loginUsecase.execute(testUser.email, testUser.password!);

    // Assert
    expect(result, Right(testLoginModel));
    verify(mockUserRepository.login(testUser.email, testUser.password!));
    verifyNoMoreInteractions(mockUserRepository);
  });

  test('should return a Failure from the repository', () async {
    // Arrange
    const tFailure = ServerFailure('Invalid credentials');
    when(mockUserRepository.login(testUser.email, testUser.password!))
        .thenAnswer((_) async => const Left(tFailure));

    // Act
    final result =
        await loginUsecase.execute(testUser.email, testUser.password!);

    // Assert
    expect(result, const Left(tFailure));
    verify(mockUserRepository.login(testUser.email, testUser.password!));
    verifyNoMoreInteractions(mockUserRepository);
  });
}
