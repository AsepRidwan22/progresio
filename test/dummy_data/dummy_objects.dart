import 'package:frontmobile_blog_app/data/model/responses/response_login_model.dart';
import 'package:frontmobile_blog_app/domain/entities/user_entity.dart';

UserEntity testUser = const UserEntity(
  name: 'Asep Ridwan',
  email: 'asepriddwan220201@gmail.com',
  nik: '1234567890123456',
  noHp: '083107113661',
  password: 'password',
  role: 'admin',
);

ResponseLoginModel testLoginModel = ResponseLoginModel(
  success: true,
  message: 'Login successful',
  user: testUser,
);
