import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:frontmobile_blog_app/data/datasources/user_remote_datasource_impl.dart';
import 'package:frontmobile_blog_app/data/helper/secure_storage_helper.dart';
import 'package:frontmobile_blog_app/data/repositories/user_repository_impl.dart';
import 'package:frontmobile_blog_app/domain/repositories/user_repository.dart';
import 'package:frontmobile_blog_app/domain/usecases/login_usecase.dart';
import 'package:frontmobile_blog_app/domain/usecases/logout_usecase.dart';
import 'package:frontmobile_blog_app/domain/usecases/register_usecase.dart';
import 'package:frontmobile_blog_app/presentation/bloc/auth/auth_bloc.dart';
import 'package:frontmobile_blog_app/presentation/bloc/auth/verification_form_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

final locator = GetIt.instance;

void init() {
  // Data Source
  locator.registerLazySingleton<UserRemoteDataSource>(
    () => UserRemoteDataSourceImpl(
      client: locator(),
      secureStorageHelper: locator(),
    ),
  );

  // Repositry
  locator.registerLazySingleton<UserRepository>(
    () => UserRepositoryImpl(
      remoteDataSource: locator(),
    ),
  );

  // Use case
  locator.registerLazySingleton(() => LoginUsecase(locator()));
  locator.registerLazySingleton(() => RegisterUsecase(locator()));
  locator.registerLazySingleton(() => LogoutUsecase(locator()));

  // BloC
  locator.registerFactory(
    () => AuthBloc(
      locator(),
      locator(),
      locator(),
    ),
  );

  locator.registerFactory(
    () => VerificationFormBloc(),
  );

  // helper
  const secureStorage = FlutterSecureStorage();
  locator.registerLazySingleton(() => SecureStorageHelper(secureStorage));

  // external
  locator.registerLazySingleton(() => http.Client());
}
