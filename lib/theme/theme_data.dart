import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

const Color primaryColor = Color(0xFF009CFF);
const Color darkerPrimaryColor = Color.fromARGB(255, 0, 125, 202);
Color primaryContainer = const Color(0xFF009CFF).withOpacity(0.1);
const Color backgroundPrimary = Color(0xFFFFFFFF);
const Color backgroundSecondary = Color(0xFFF8F8F8);
const Color outlineGrey = Color(0xFFdadfe4);
const Color grey = Color(0xFF5A6676);
const Color colorError = Color(0xFFFF4238);
const Color primaryText = Color(0xFF000000);
const Color secondaryText = Color(0xFF9299A9);
const Color bStroke = Color.fromARGB(30, 0, 0, 0);
const Color backgroundWhite = Color.fromARGB(255, 255, 255, 255);

ThemeData lightTheme = ThemeData(
  brightness: Brightness.light,
  scaffoldBackgroundColor: backgroundWhite,
  primaryColor: primaryColor,
  colorScheme: ColorScheme(
    brightness: Brightness.light,
    // button color primary
    primary: primaryColor,
    onPrimary: backgroundPrimary,
    // container color primary
    primaryContainer: backgroundPrimary,
    onPrimaryContainer: backgroundPrimary,
    // button secondary
    secondary: backgroundPrimary,
    onSecondary: primaryColor,
    // text color primary
    tertiary: primaryText,
    // text color secondary
    onTertiary: outlineGrey,
    // text color primary in container
    tertiaryContainer: primaryColor,
    // text color secondary in container
    onTertiaryContainer: grey,
    // error
    error: colorError,
    onError: primaryColor,
    // error in container
    errorContainer: colorError,
    onErrorContainer: primaryColor,
    // // background color
    background: backgroundPrimary,
    onBackground: primaryColor,
    // // card primary
    surface: backgroundPrimary,
    onSurface: outlineGrey,
    secondaryContainer: primaryContainer,
    shadow: bStroke,
    onSecondaryContainer: backgroundSecondary,
    scrim: secondaryText,
  ),
  inputDecorationTheme: InputDecorationTheme(
    contentPadding: const EdgeInsets.all(16),
    floatingLabelBehavior: FloatingLabelBehavior.never,
    errorBorder: bBorderBuilder(colorError),
    focusedErrorBorder: bBorderBuilder(colorError),
    focusedBorder: bBorderBuilder(primaryColor),
    disabledBorder: bBorderBuilder(bStroke),
    filled: true,
    fillColor: backgroundPrimary,
    labelStyle: bSubtitle1.copyWith(color: grey),
    hintStyle: bSubtitle1.copyWith(color: outlineGrey),
    prefixIconColor: grey,
    suffixIconColor: grey,
    border: bBorderBuilder(primaryColor),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
    style: ButtonStyle(
      backgroundColor: MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.pressed)) {
            return darkerPrimaryColor;
          }
          return primaryColor;
        },
      ),
      textStyle: MaterialStateProperty.all(
        bButton1,
      ),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4),
        ),
      ),
      alignment: Alignment.center,
      padding:
          MaterialStateProperty.all(const EdgeInsets.symmetric(horizontal: 16)),
    ),
  ),
  outlinedButtonTheme: OutlinedButtonThemeData(
    style: OutlinedButton.styleFrom(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
      ),
      side: const BorderSide(
        color: outlineGrey,
        width: 1.0,
      ),
      alignment: Alignment.center,
      padding: const EdgeInsets.all(20.0),
    ),
  ),
  dialogTheme: DialogTheme(
    backgroundColor: backgroundPrimary,
    surfaceTintColor: backgroundPrimary,
    titleTextStyle: bHeading1.copyWith(color: primaryText),
    contentTextStyle: bSubtitle2.copyWith(color: secondaryText),
    actionsPadding: const EdgeInsets.all(10),
  ),
  checkboxTheme: CheckboxThemeData(
    side: const BorderSide(
      width: 2.0,
      color: primaryColor,
    ),
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(5),
    ),
    fillColor: MaterialStateProperty.all(primaryColor),
  ),
);

OutlineInputBorder bBorderBuilder(Color color) {
  return OutlineInputBorder(
    borderRadius: const BorderRadius.all(Radius.circular(3)),
    borderSide: BorderSide(
      color: color,
      width: 1.0,
    ),
  );
}

// text style
final TextStyle bHeading0 =
    GoogleFonts.openSans(fontSize: 32, fontWeight: FontWeight.w600);
final TextStyle bHeading1 =
    GoogleFonts.openSans(fontSize: 22, fontWeight: FontWeight.w600);
final TextStyle bHeading2 =
    GoogleFonts.openSans(fontSize: 20, fontWeight: FontWeight.w600);
final TextStyle bHeading3 =
    GoogleFonts.openSans(fontSize: 20, fontWeight: FontWeight.w700);
final TextStyle bHeading4 =
    GoogleFonts.openSans(fontSize: 16, fontWeight: FontWeight.w600);
final TextStyle bSubtitle1 =
    GoogleFonts.openSans(fontSize: 14, fontWeight: FontWeight.w600);
final TextStyle bSubtitle2 =
    GoogleFonts.openSans(fontSize: 14, fontWeight: FontWeight.w400);
final TextStyle bSubtitle3 =
    GoogleFonts.openSans(fontSize: 13, fontWeight: FontWeight.w600);
final TextStyle bSubtitle4 =
    GoogleFonts.openSans(fontSize: 13, fontWeight: FontWeight.w400);
final TextStyle bBody1 =
    GoogleFonts.openSans(fontSize: 12, fontWeight: FontWeight.w600);
final TextStyle bBody2 =
    GoogleFonts.openSans(fontSize: 12, fontWeight: FontWeight.w400);
final TextStyle bBody3 =
    GoogleFonts.openSans(fontSize: 10, fontWeight: FontWeight.w400);
final TextStyle bButton1 =
    GoogleFonts.openSans(fontSize: 16, fontWeight: FontWeight.w600);
