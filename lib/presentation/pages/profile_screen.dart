import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontmobile_blog_app/common/auth_state_enum.dart';
import 'package:frontmobile_blog_app/presentation/bloc/auth/auth_bloc.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_button.dart';
import 'package:frontmobile_blog_app/presentation/widget/my_clipper.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      body: BlocListener<AuthBloc, AuthState>(
        listener: (context, state) {
          if (state.authStateEnum == AuthStateEnum.isInitial) {
            Navigator.pushReplacementNamed(
              context,
              '/login',
            );
          }
        },
        child: Stack(
          children: [
            _buildBackgroundRounded(context),
            _buildTexture(),
            CustomScrollView(
              physics: const BouncingScrollPhysics(),
              slivers: [
                SliverAppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: Colors.transparent,
                  flexibleSpace: _customAppBar(context),
                ),
                BlocBuilder<AuthBloc, AuthState>(
                  builder: (context, state) {
                    return SliverToBoxAdapter(
                      child: _showProfile(
                        context,
                        state.currentUser.name,
                        state.currentUser.email,
                        state.currentUser.noHp,
                        state.currentUser.nik,
                      ),
                    );
                  },
                ),
                SliverToBoxAdapter(
                  child: _showWorkspace(
                    context,
                    'assets/images/logo.png',
                    'label',
                    10,
                  ),
                ),
                SliverToBoxAdapter(
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Form Saya',
                            style: bHeading4.copyWith(
                              color: colorScheme.tertiary,
                            ),
                          ),
                        ),
                        _showFormMe(
                          context,
                          'assets/images/FotoTanpaKumis.jpg',
                          'Elektabilitas Paslon Kaltim',
                          'Amet minim mollit non deserunt est sit sdfsd sdfsd',
                          '23/03/22 16:36',
                          'klien',
                          () {},
                        ),
                        _showFormMe(
                          context,
                          'assets/images/FotoTanpaKumis.jpg',
                          'Elektabilitas Paslon Kaltim',
                          'Amet minim mollit non deserunt est sit sdfsd sdfsd',
                          '23/03/22 16:36',
                          'klien',
                          () {},
                        ),
                        _showFormMe(
                          context,
                          'assets/images/FotoTanpaKumis.jpg',
                          'Elektabilitas Paslon Kaltim',
                          'Amet minim mollit non deserunt est sit sdfsd sdfsd',
                          '23/03/22 16:36',
                          'klien',
                          () {},
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

Widget _showFormMe(BuildContext context, String img, String title,
    String subTitle, String date, String labelBtn, Function() funBtn) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;
  return Column(
    children: [
      const SizedBox(
        height: 12,
      ),
      Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 14,
          vertical: 12,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5),
          border: Border.all(
            color: colorScheme.onTertiary,
            width: 1,
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 60,
              width: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(4),
                image: DecorationImage(
                  image: AssetImage(img),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: bSubtitle1.copyWith(
                      color: colorScheme.tertiary,
                    ),
                  ),
                  Text(
                    subTitle,
                    style: bBody2.copyWith(
                      color: colorScheme.scrim,
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        date,
                        style: bBody2.copyWith(
                          color: colorScheme.scrim,
                        ),
                      ),
                      GestureDetector(
                        onTap: funBtn,
                        child: Text(
                          labelBtn,
                          style: bBody1.copyWith(
                            color: colorScheme.primary,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    ],
  );
}

Widget _buildBackgroundRounded(BuildContext context) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;
  return ClipPath(
    clipper: MyClipper(),
    child: Container(
      color: colorScheme.primary,
      width: MediaQuery.of(context).size.width,
      height: 305,
    ),
  );
}

Widget _buildTexture() {
  return Positioned.fill(
    bottom: 900,
    child: OverflowBox(
      minWidth: 0.0,
      maxWidth: double.infinity,
      minHeight: 0.0,
      maxHeight: double.infinity,
      child: SvgPicture.asset(
        "assets/backgrounds/textureBg.svg",
        width: 620,
      ),
    ),
  );
}

Widget _showWorkspace(
    BuildContext context, String img, String label, int item) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;
  return Padding(
    padding: const EdgeInsets.only(
      top: 40,
      bottom: 20,
    ),
    child: Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Workspace Saya',
                style: bHeading4.copyWith(
                  color: colorScheme.tertiary,
                ),
              ),
              CustomTextButton(
                child: Text(
                  'Undangan',
                  style: bSubtitle1.copyWith(
                    color: colorScheme.primary,
                  ),
                ),
                onTap: () {},
              )
            ],
          ),
        ),
        Container(
          height: 81,
          margin: const EdgeInsets.only(top: 10),
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: item,
            // itemExtent: 90,
            itemBuilder: (context, index) {
              EdgeInsets itemPadding = const EdgeInsets.only(right: 16);
              if (index == 0) {
                itemPadding = const EdgeInsets.only(right: 16, left: 20);
              }
              return Padding(
                padding: itemPadding,
                child: Column(
                  children: [
                    Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: AssetImage(img),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Text(
                      label,
                      style: bBody2.copyWith(
                        color: colorScheme.tertiary,
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ],
    ),
  );
}

Widget _customAppBar(BuildContext context) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 40,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  color: colorScheme.onPrimary.withOpacity(0.1),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Center(
                  child: SvgPicture.asset(
                    "assets/icon/backWhite.svg",
                    height: 20.0,
                  ),
                ),
              ),
            ),
            BlocBuilder<AuthBloc, AuthState>(
              builder: (context, state) {
                return GestureDetector(
                  onTap: () {
                    // Tampilkan modal konfirmasi logout
                    showDialog(
                      context: context,
                      builder: (BuildContext dialogContext) {
                        return _showDialogLogout(
                          context,
                          dialogContext,
                        );
                      },
                    );
                  },
                  child: Container(
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: colorScheme.onPrimary.withOpacity(0.1),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Center(
                      child: SvgPicture.asset(
                        "assets/icon/logout.svg",
                        height: 25.0,
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ],
    ),
  );
}

Widget _showDialogLogout(BuildContext context, BuildContext dialogContext) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;
  return AlertDialog(
    title: const Text('Konfirmasi Logout'),
    content: const Text('Apakah Anda yakin ingin logout?'),
    actions: [
      TextButton(
        onPressed: () {
          Navigator.of(dialogContext).pop(); // Tutup modal
        },
        child: Text(
          'Batal',
          style: bButton1.copyWith(
            color: colorScheme.onTertiaryContainer,
          ),
        ),
      ),
      TextButton(
        onPressed: () {
          Navigator.of(dialogContext).pop(); // Tutup modal
          context.read<AuthBloc>().add(const OnAuthLogout());
        },
        child: Text(
          'Logout',
          style: bButton1.copyWith(
            color: colorScheme.error,
          ),
        ),
      ),
    ],
  );
}

Widget _showProfile(
    BuildContext context, String name, String email, String noHP, String nik) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: MainAxisAlignment.start,
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            padding: const EdgeInsets.symmetric(
              vertical: 12,
              horizontal: 28,
            ),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: colorScheme.onPrimary.withOpacity(0.3),
            ),
            child: SvgPicture.asset(
              'assets/icon/bigProfile.svg',
            ),
          ),
          const SizedBox(
            width: 5,
          ),
          SvgPicture.asset(
            'assets/icon/edit.svg',
            height: 16,
          ),
        ],
      ),
      const SizedBox(
        height: 8,
      ),
      Text(
        name,
        style: bHeading4.copyWith(
          color: colorScheme.onPrimary,
        ),
      ),
      const SizedBox(
        height: 12,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            children: [
              SvgPicture.asset(
                'assets/icon/email.svg',
                height: 16,
              ),
              const SizedBox(
                width: 4,
              ),
              Text(
                email,
                style: bBody2.copyWith(
                  color: colorScheme.onPrimary,
                ),
              ),
            ],
          ),
          Container(
            width: 1.0,
            height: 16,
            color: Colors.white,
            margin: const EdgeInsets.symmetric(
              horizontal: 8,
            ),
          ),
          Row(
            children: [
              SvgPicture.asset(
                'assets/icon/telephone.svg',
                height: 16,
              ),
              const SizedBox(
                width: 4,
              ),
              Text(
                noHP,
                style: bBody2.copyWith(
                  color: colorScheme.onPrimary,
                ),
              ),
            ],
          ),
        ],
      ),
      const SizedBox(
        height: 9,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SvgPicture.asset(
            'assets/icon/id.svg',
            height: 16,
          ),
          const SizedBox(
            width: 4,
          ),
          Text(
            nik,
            style: bBody2.copyWith(
              color: colorScheme.onPrimary,
            ),
          ),
        ],
      )
    ],
  );
}
