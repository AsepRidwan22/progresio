import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_search_filter_app_bar.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_secondary_icon_text_button.dart';
import 'package:frontmobile_blog_app/presentation/widget/search_filter_bottom_sheet.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class SearchResultFilterScreen extends StatelessWidget {
  const SearchResultFilterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return [
            SliverAppBar(
              systemOverlayStyle: const SystemUiOverlayStyle(
                statusBarColor: Colors.transparent,
                statusBarBrightness: Brightness.light,
                statusBarIconBrightness: Brightness.light,
              ),
              backgroundColor: colorScheme.primary,
              toolbarHeight: 86,
              automaticallyImplyLeading: false,
              flexibleSpace: CustomSearchFilterAppBar(
                onTap: () {},
              ),
              surfaceTintColor: colorScheme.primary,
              pinned: true,
            ),
          ];
        },
        body: CustomScrollView(
          physics: const BouncingScrollPhysics(),
          slivers: [
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  Container(
                    padding: const EdgeInsets.only(top: 16, bottom: 16),
                    // decoration: BoxDecoration(
                    //   color: colorScheme.onPrimary,
                    // ),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 20,
                          ),
                          CustomSecondaryIconTextButton(
                            onTap: () {
                              showModalBottomSheet(
                                context: context,
                                isScrollControlled: true,
                                builder: searchFilterBottomSheet,
                              );
                            },
                            buttonColor: colorScheme.onPrimary,
                            borderRadius: 6,
                            borderColor: colorScheme.onSurface,
                            borderWidth: 1,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                  "assets/icon/filterBlack.svg",
                                  height: 12.0,
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.symmetric(horizontal: 5),
                                  child: Text(
                                    'Filter',
                                    style: bSubtitle4.copyWith(
                                      color: colorScheme.tertiary,
                                    ),
                                  ),
                                ),
                                Container(
                                  padding: const EdgeInsets.all(5),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: colorScheme.primary),
                                  child: Center(
                                    child: Text(
                                      '5', // Ganti dengan angka yang diinginkan
                                      style: bBody3.copyWith(
                                          color: colorScheme.onPrimary),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4),
                            child: _filterValue(
                              context,
                              firstDate: '2022-01-01',
                              lastDate: '2022-01-31',
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4),
                            child: _filterValue(
                              context,
                              value: 'Tanggal',
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4),
                            child: _filterValue(
                              context,
                              value: 'Tugas',
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 4),
                            child: _filterValue(
                              context,
                              value: 'Kepemilikan',
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(right: 20, left: 20, bottom: 16),
                    child: Text(
                      'Hasil Pencarian',
                      style: bHeading4.copyWith(color: colorScheme.tertiary),
                    ),
                  ),
                  _listProfile(
                    context,
                    'assets/images/FotoTanpaKumis.jpg',
                    'Brooklyn Simmons',
                    'Safarudin',
                    '29 Maret 2021 14:30',
                  ),
                  _listProfile(
                    context,
                    'assets/images/FotoTanpaKumis.jpg',
                    'Brooklyn Simmons',
                    'Safarudin',
                    '29 Maret 2021 14:30',
                  ),
                  _listProfile(
                    context,
                    'assets/images/FotoTanpaKumis.jpg',
                    'Brooklyn Simmons',
                    'Safarudin',
                    '29 Maret 2021 14:30',
                  ),
                  _listProfile(
                    context,
                    'assets/images/FotoTanpaKumis.jpg',
                    'Brooklyn Simmons',
                    'Safarudin',
                    '29 Maret 2021 14:30',
                  ),
                  _listProfile(
                    context,
                    'assets/images/FotoTanpaKumis.jpg',
                    'Brooklyn Simmons',
                    'Safarudin',
                    '29 Maret 2021 14:30',
                  ),
                  _listProfile(
                    context,
                    'assets/images/FotoTanpaKumis.jpg',
                    'Brooklyn Simmons',
                    'Safarudin',
                    '29 Maret 2021 14:30',
                  ),
                  _listProfile(
                    context,
                    'assets/images/FotoTanpaKumis.jpg',
                    'Brooklyn Simmons',
                    'Safarudin',
                    '29 Maret 2021 14:30',
                  ),
                  _listProfile(
                    context,
                    'assets/images/FotoTanpaKumis.jpg',
                    'Brooklyn Simmons',
                    'Safarudin',
                    '29 Maret 2021 14:30',
                  ),
                  _listProfile(
                    context,
                    'assets/images/FotoTanpaKumis.jpg',
                    'Brooklyn Simmons',
                    'Safarudin',
                    '29 Maret 2021 14:30',
                  ),
                  _listProfile(
                    context,
                    'assets/images/FotoTanpaKumis.jpg',
                    'Brooklyn Simmons',
                    'Safarudin',
                    '29 Maret 2021 14:30',
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget _filterValue(BuildContext context,
    {String? firstDate, String? lastDate, String? value}) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;

  return Container(
    // margin: const EdgeInsets.only(left: 6),
    padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(6),
      border: Border.all(
        color: colorScheme.primary,
        width: 1,
      ),
      color: colorScheme.secondaryContainer,
    ),
    child: Text(
      (firstDate != null && lastDate != null)
          ? '$firstDate - $lastDate'
          : value!,
      style: bSubtitle2.copyWith(color: colorScheme.primary),
    ),
  );
}

Widget _listProfile(BuildContext context, String img, String labelName,
    String subName, String date) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;
  return Container(
    padding: const EdgeInsets.symmetric(horizontal: 20),
    child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            CircleAvatar(
              radius: 16, // Ukuran radius gambar bulat
              backgroundImage: AssetImage(
                img,
              ),
            ),
            const SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    labelName,
                    style: bSubtitle1.copyWith(color: colorScheme.tertiary),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        subName,
                        style: bBody1.copyWith(
                          color: colorScheme.onTertiaryContainer,
                        ),
                      ),
                      Text(
                        date,
                        style: bBody2.copyWith(
                          color: colorScheme.onTertiaryContainer,
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16),
          child: Container(
            height: 1.0,
            color: colorScheme.onTertiary,
          ),
        ),
      ],
    ),
  );
}
