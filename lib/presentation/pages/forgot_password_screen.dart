import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontmobile_blog_app/common/regex_validation_field.dart';
import 'package:frontmobile_blog_app/presentation/bloc/auth/verification_form_bloc.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_app_bar.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_head_sub_text.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_primary_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_form_field.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class ForgotPasswordScreen extends StatelessWidget {
  const ForgotPasswordScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    // AuthBloc authBloc = context.read<AuthBloc>();

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: CustomAppBar(
                onTap: () {},
              ),
              surfaceTintColor: colorScheme.onPrimary,
              pinned: true,
            ),
            BlocBuilder<VerificationFormBloc, VerificationFormState>(
              builder: (context, forgotPasswordState) {
                return Form(
                  key: formKey,
                  child: SliverPadding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: MediaQuery.of(context).size.height * 0.08,
                    ),
                    sliver: SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          SvgPicture.asset(
                            "assets/images/forgotPassword.svg",
                            height: 150.0,
                          ),
                          const SizedBox(height: 32),
                          const CustomHeadSubText(
                            mainText: 'Lupa Sandi?',
                            subText:
                                'Kode Konfirmasi akan dikirimkan ke alamat emailmu',
                          ),
                          CustomTextFormField(
                            labelText: 'Email',
                            isRequired: true,
                            hintText: 'Masukan Email',
                            initialValue: '',
                            validator: validateEmail,
                            onChanged: (text) {
                              // authBloc.add(AuthFormEmailChanged(email: text));
                            },
                          ),
                          CustomPrimaryButton(
                            width: MediaQuery.of(context).size.width,
                            onTap: () {
                              // if (formKey.currentState?.validate() == true) {
                              //   authBloc.add(const OnAuthLoginUser());
                              // }
                              Navigator.pushNamed(
                                  context, '/code-verification');
                            },
                            child: (forgotPasswordState.isLoading == true)
                                ? SizedBox(
                                    width: 20,
                                    height: 20,
                                    child: CircularProgressIndicator(
                                      color: colorScheme.onPrimary,
                                    ),
                                  )
                                : Text(
                                    'Kirim',
                                    style: bButton1.copyWith(
                                        color: colorScheme.onPrimary),
                                  ),
                          ),
                          const SizedBox(height: 130),
                          CustomTextButton(
                            onTap: () {
                              Navigator.pushNamed(context, '/login');
                            },
                            child: Text(
                              'Kembali ke Masuk',
                              style: bSubtitle3.copyWith(
                                  color: colorScheme.primary),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
