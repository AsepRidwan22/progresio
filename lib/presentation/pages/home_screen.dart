import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_secondary_icon_text_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_button.dart';
import 'package:frontmobile_blog_app/presentation/widget/my_clipper.dart';
import 'package:frontmobile_blog_app/presentation/widget/search_filter_bottom_sheet.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          ClipPath(
            clipper: MyClipper(),
            child: Container(
              color: colorScheme.primary,
              width: MediaQuery.of(context).size.width,
              height: 305,
            ),
          ),
          Positioned.fill(
            bottom: 900,
            child: OverflowBox(
              minWidth: 0.0,
              maxWidth: double.infinity,
              minHeight: 0.0,
              maxHeight: double.infinity,
              child: SvgPicture.asset(
                "assets/backgrounds/textureBg.svg",
                width: 620,
              ),
            ),
          ),
          CustomScrollView(
            physics: const BouncingScrollPhysics(),
            slivers: [
              SliverAppBar(
                automaticallyImplyLeading: false,
                backgroundColor: Colors.transparent,
                flexibleSpace: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 40,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          CustomSecondaryIconTextButton(
                            onTap: () {
                              showModalBottomSheet(
                                context: context,
                                isScrollControlled: true,
                                builder: searchFilterBottomSheet,
                              );
                            },
                            buttonColor: colorScheme.onPrimary.withOpacity(0.3),
                            borderRadius: 6,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SvgPicture.asset(
                                  "assets/icon/filter.svg",
                                  height: 12.0,
                                ),
                                const SizedBox(
                                  width: 4,
                                ),
                                Text(
                                  'Filter',
                                  style: bSubtitle4.copyWith(
                                    color: colorScheme.onPrimary,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Row(
                            children: [
                              CustomTextButton(
                                child: SvgPicture.asset(
                                  "assets/icon/bell.svg",
                                  height: 30.0,
                                ),
                                onTap: () {},
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              CustomSecondaryIconTextButton(
                                onTap: () {
                                  Navigator.pushNamed(
                                    context,
                                    '/profile',
                                  );
                                },
                                buttonColor:
                                    colorScheme.onPrimary.withOpacity(0.3),
                                borderRadius: 6,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SvgPicture.asset(
                                      "assets/icon/profile.svg",
                                      height: 24.0,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      // const SizedBox(
                      //   height: 14,
                      // ),
                    ],
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  padding: const EdgeInsets.only(
                    right: 20,
                    left: 20,
                    bottom: 16,
                  ),
                  child: Text(
                    'Hi, Michael',
                    style: bHeading3.copyWith(color: colorScheme.onPrimary),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  padding: const EdgeInsets.only(
                    left: 20,
                    right: 20,
                    bottom: 20,
                  ),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        '/search-filter',
                      );
                    },
                    child: Container(
                      constraints: const BoxConstraints(
                        minHeight: 40.0,
                        maxHeight: 40.0,
                      ),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: colorScheme.onPrimary,
                        borderRadius: BorderRadius.circular(4.0),
                      ),
                      padding: const EdgeInsets.all(12),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SvgPicture.asset(
                            'assets/icon/search.svg',
                            height: 15,
                          ),
                          const SizedBox(
                            width: 6,
                          ),
                          Text(
                            'Search',
                            style: bSubtitle2.copyWith(
                              color: colorScheme.scrim,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      const SizedBox(
                        width: 20,
                      ),
                      _menu(
                        context,
                        "assets/images/lastReport.svg",
                        "45",
                        'Laporan Terakhir',
                      ),
                      _menu(
                        context,
                        "assets/images/myReport.svg",
                        "45",
                        'Report Saya',
                      ),
                      _menu(
                        context,
                        "assets/images/draft.svg",
                        "45",
                        'Draft',
                      ),
                      _menu(
                        context,
                        "assets/images/activeForm.svg",
                        "45",
                        'Form Aktif',
                      ),
                    ],
                  ),
                ),
              ),
            ], // Gantilah dengan widget tambahan Anda
          ),
          DraggableScrollableSheet(
            initialChildSize: 0.62,
            minChildSize: 0.62,
            maxChildSize: 0.9,
            builder: (BuildContext context, ScrollController scrollController) {
              ColorScheme colorScheme = Theme.of(context).colorScheme;
              return Container(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                decoration: BoxDecoration(
                  color: colorScheme.onPrimary,
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(10.0),
                    topRight: Radius.circular(10.0),
                  ),
                ),
                child: CustomScrollView(
                  controller: scrollController,
                  slivers: [
                    SliverToBoxAdapter(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: colorScheme.onPrimary,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                            // horizontal: 20,
                            vertical: 10,
                          ),
                          child: Text(
                            'Laporan Terakhir',
                            style:
                                bHeading4.copyWith(color: colorScheme.tertiary),
                          ),
                        ),
                      ),
                    ),
                    SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                          return _listReport(
                            context,
                            'Andri',
                            'Amet minim mollit non deserunt ullamco est sit... ',
                            '23/03/22 16:36',
                            '2021103923',
                          );
                        },
                        childCount: 10,
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ],
      ),
    );
  }
}

Widget _menu(
    BuildContext context, String icon, String value, String labelText) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;
  return Row(
    children: [
      Container(
        height: 120,
        width: 250,
        margin: const EdgeInsets.only(right: 20.0),
        padding: const EdgeInsets.only(top: 16, bottom: 10, right: 20),
        decoration: BoxDecoration(
          color: colorScheme.onPrimary,
          border: Border.all(
            color: colorScheme.onSurface,
            width: 1.0,
          ),
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              icon,
              height: 85.0,
            ),
            const SizedBox(width: 5.0),
            Container(
              width: 120,
              margin: const EdgeInsets.only(bottom: 14),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    value,
                    style: bHeading0.copyWith(color: colorScheme.tertiary),
                  ),
                  Text(
                    labelText,
                    style: bSubtitle2.copyWith(
                      color: colorScheme.tertiary.withOpacity(0.7),
                    ),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 4,
            )
          ],
        ),
      ),
    ],
  );
}

Widget _listReport(
  BuildContext context,
  String name,
  String desc,
  String date,
  String no,
) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Container(
        decoration: BoxDecoration(
          color: colorScheme.onPrimary,
          border: Border.all(
            color: colorScheme.onSurface,
            width: 1.0,
          ),
          borderRadius: BorderRadius.circular(4.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(14.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: const EdgeInsets.all(6),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    color: colorScheme.onSurface,
                    width: 1, // Lebar border
                  ),
                ),
                child: Center(
                  child: SvgPicture.asset(
                    "assets/icon/papper.svg",
                    height: 12.0,
                  ),
                ),
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          name,
                          style: bSubtitle1.copyWith(
                            color: colorScheme.tertiary,
                          ),
                        ),
                        Text(
                          no,
                          style: bBody1.copyWith(
                            color: colorScheme.primary,
                          ),
                        ),
                      ],
                    ),
                    Text(
                      desc,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: bBody1.copyWith(
                        color: colorScheme.onTertiaryContainer,
                      ),
                    ),
                    Text(
                      date,
                      overflow: TextOverflow.ellipsis,
                      style: bBody1.copyWith(
                        color: colorScheme.onTertiaryContainer,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      const SizedBox(
        height: 16,
      ),
    ],
  );
}
