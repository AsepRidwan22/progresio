import 'package:frontmobile_blog_app/common/form_status_enum.dart';
import 'package:frontmobile_blog_app/common/regex_validation_field.dart';
import 'package:frontmobile_blog_app/presentation/bloc/auth/auth_bloc.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_checkbox.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_head_sub_text.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_primary_icon_text_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_primary_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_form_field.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_password_text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:frontmobile_blog_app/presentation/widget/divider_or_widget.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({super.key});
  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      // appBar: AppBar(
      //   backgroundColor: Colors.deepPurple,
      //   foregroundColor: Colors.white,
      //   title: const Text('Register'),
      // ),
      resizeToAvoidBottomInset: true,
      body: BlocListener<AuthBloc, AuthState>(
        listenWhen: (previous, current) {
          if (previous.formStatus == FormStatusEnum.submittingForm) {
            debugPrint('field belum diisi');
            return true;
          } else {
            debugPrint('field sudah diisi');
            return false;
          }
        },
        listener: (context, state) {
          if (state.formStatus == FormStatusEnum.failedSubmission) {
            state.message == 'Failed to Connect to the Network'
                ? Navigator.pushNamed(
                    context,
                    '/no-connection',
                  )
                : ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(state.message),
                      backgroundColor: colorScheme.error,
                    ),
                  );
            debugPrint('field register gagal');
          } else if (state.formStatus == FormStatusEnum.successSubmission) {
            Future(() {
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text(state.message),
                  backgroundColor: Colors.green,
                ),
              );
            }).then((value) {
              Navigator.pushNamed(
                context,
                '/login',
              );
            });
            debugPrint('field register berhasil');
          }
        },
        child: const SafeArea(
            child: SingleChildScrollView(child: RegisterFormFields())),
      ),
    );
  }
}

class RegisterFormFields extends StatelessWidget {
  const RegisterFormFields({super.key});

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Container(
      padding: EdgeInsets.symmetric(
          horizontal: 20, vertical: MediaQuery.of(context).size.height * 0.05),
      child: BlocBuilder<AuthBloc, AuthState>(
        builder: (context, registerForm) {
          return Form(
            key: formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const CustomHeadSubText(
                  mainText: 'Buat Akun',
                  subText: 'Daftar sekarang untuk memulai akunmu',
                ),
                CustomPrimaryIconTextButton(
                  width: MediaQuery.of(context).size.width,
                  text: 'Masuk dengan Google',
                  icon: 'assets/icon/iconGoogle.svg',
                  onTap: () {},
                ),
                dividOrWidget(),
                CustomTextFormField(
                  labelText: 'Nama',
                  hintText: 'Masukan Nama',
                  initialValue: registerForm.name,
                  validator: validateName,
                  isRequired: true,
                  onChanged: (text) {
                    context
                        .read<AuthBloc>()
                        .add(AuthFormNameChanged(name: text));
                  },
                ),
                CustomTextFormField(
                  labelText: 'Email',
                  hintText: 'Masukan Email',
                  initialValue: registerForm.emailRegister,
                  textInputType: TextInputType.emailAddress,
                  validator: validateEmail,
                  isRequired: true,
                  onChanged: (text) {
                    context
                        .read<AuthBloc>()
                        .add(AuthFormEmailRegisterChanged(email: text));
                  },
                ),
                CustomTextFormField(
                  labelText: 'NIK',
                  hintText: 'Masukan NIK',
                  initialValue: registerForm.nik,
                  validator: validateNik,
                  textInputType: TextInputType.number,
                  isRequired: true,
                  onChanged: (text) {
                    context.read<AuthBloc>().add(AuthFormNikChanged(nik: text));
                  },
                ),
                CustomTextFormField(
                  labelText: 'Nomor Handphone',
                  hintText: 'Masukan Nomor Handphone',
                  initialValue: registerForm.noHp,
                  textInputType: TextInputType.number,
                  validator: validatePhoneNumber,
                  isRequired: true,
                  onChanged: (text) {
                    context
                        .read<AuthBloc>()
                        .add(AuthFormNoHpChanged(noHp: text));
                  },
                ),
                CustomPasswordTextFormField(
                  labelText: 'Sandi',
                  isRequired: true,
                  hintText: 'Masukan Sandi',
                  initialValue: registerForm.passwordRegister,
                  suffixIcon: PasswordSuffixIcon(
                    obscurePassword: registerForm.obsecurePassword,
                    onTap: () => context.read<AuthBloc>().add(
                          AuthFormObsecurePasswordChanged(
                            obsecure: !registerForm.obsecurePassword,
                          ),
                        ),
                  ),
                  validator: validatePassword,
                  obscureText: registerForm.obsecurePassword,
                  onChanged: (text) => context
                      .read<AuthBloc>()
                      .add(AuthFormPasswordRegiserChanged(password: text)),
                ),
                CustomPasswordTextFormField(
                  labelText: 'Ulang Sandi',
                  isRequired: true,
                  hintText: 'Masukkan Ulang Sandi',
                  initialValue: registerForm.passwordConf,
                  suffixIcon: PasswordSuffixIcon(
                    obscurePassword: registerForm.obsecurePasswordConf,
                    onTap: () => context.read<AuthBloc>().add(
                          AuthFormObsecureConfPasswordChanged(
                            obsecure: !registerForm.obsecurePasswordConf,
                          ),
                        ),
                  ),
                  validator: (item) => item!.isEmpty
                      ? 'Password tidak boleh kosong'
                      : registerForm.password == registerForm.passwordConf
                          ? 'Password harus identik dengan yang di atas'
                          : null,
                  obscureText: registerForm.obsecurePasswordConf,
                  onChanged: (text) => context.read<AuthBloc>().add(
                        AuthFormPasswordConfChanged(passwordConf: text),
                      ),
                ),
                Row(
                  children: [
                    CustomCheckbox(
                      isChecked: registerForm.isTerms,
                      labelText: Text(
                        'I have read and agree to the',
                        style: bSubtitle3.copyWith(
                            color: colorScheme.onTertiaryContainer),
                      ),
                      onChanged: (value) {
                        context.read<AuthBloc>().add(
                              AuthFormIsTerms(
                                isTerms: value!,
                              ),
                            );
                      },
                    ),
                    CustomTextButton(
                      onTap: () {
                        Navigator.pushNamed(context, '/register');
                      },
                      child: Text(
                        'Terms of Service',
                        style: bSubtitle3.copyWith(color: colorScheme.primary),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 16),
                CustomPrimaryButton(
                  width: MediaQuery.of(context).size.width,
                  onTap: () {
                    if (formKey.currentState?.validate() == true) {
                      context.read<AuthBloc>().add(const OnAuthRegisterUser());
                    }
                  },
                  child: (registerForm.isLoading == true)
                      ? SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(
                            color: colorScheme.onPrimary,
                          ),
                        )
                      : Text(
                          'Submit',
                          style:
                              bButton1.copyWith(color: colorScheme.onPrimary),
                        ),
                ),
                const SizedBox(
                  height: 16,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Already have an account?',
                      style: bSubtitle4.copyWith(color: colorScheme.tertiary),
                    ),
                    CustomTextButton(
                      onTap: () {
                        Navigator.pushNamed(context, '/login');
                      },
                      child: Text(
                        'Log in',
                        style: bSubtitle3.copyWith(color: colorScheme.primary),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
