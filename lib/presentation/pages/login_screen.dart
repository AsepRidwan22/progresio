import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:frontmobile_blog_app/common/form_status_enum.dart';
import 'package:frontmobile_blog_app/common/regex_validation_field.dart';
import 'package:frontmobile_blog_app/presentation/bloc/auth/auth_bloc.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_head_sub_text.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_primary_icon_text_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_password_text_form_field.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_primary_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_form_field.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_checkbox.dart';
import 'package:frontmobile_blog_app/presentation/widget/divider_or_widget.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final formKey = GlobalKey<FormState>();
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: BlocListener<AuthBloc, AuthState>(
        listenWhen: (previous, current) =>
            previous.formStatus == FormStatusEnum.submittingForm,
        listener: (context, state) {
          if (state.formStatus == FormStatusEnum.failedSubmission) {
            // print('message dari state ${state.message}');
            state.message == 'Failed to Connect to the Network'
                ? Navigator.pushNamed(
                    context,
                    '/no-connection',
                  )
                : ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                      content: Text(state.message),
                      backgroundColor: colorScheme.error,
                    ),
                  );
          } else if (state.formStatus == FormStatusEnum.successSubmission) {
            Navigator.pushReplacementNamed(
              context,
              '/home',
            );
          }
        },
        child: CustomScrollView(
          slivers: <Widget>[
            const SliverAppBar(
              automaticallyImplyLeading: false,
              systemOverlayStyle: SystemUiOverlayStyle(
                statusBarBrightness: Brightness.dark,
                statusBarColor: Colors.white,
                statusBarIconBrightness: Brightness.dark,
              ),
            ),
            BlocBuilder<AuthBloc, AuthState>(
              builder: (context, loginForm) {
                debugPrint('cek state email ${loginForm.email}');
                return Form(
                  key: formKey,
                  child: SliverPadding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                      vertical: MediaQuery.of(context).size.height * 0.08,
                    ),
                    sliver: SliverList(
                      delegate: SliverChildListDelegate([
                        const CustomHeadSubText(
                          mainText: 'Masuk ke dalam akunmu',
                          subText: 'Selamat Datang, Silahkan Isi Detil Anda',
                        ),
                        CustomPrimaryIconTextButton(
                          width: MediaQuery.of(context).size.width,
                          text: 'Masuk dengan Google',
                          icon: 'assets/icon/iconGoogle.svg',
                          onTap: () {},
                        ),
                        dividOrWidget(),
                        CustomTextFormField(
                          labelText: 'Email',
                          hintText: 'Masukan Email',
                          initialValue: loginForm.email,
                          validator: validateEmail,
                          textInputType: TextInputType.emailAddress,
                          onChanged: (text) {
                            context
                                .read<AuthBloc>()
                                .add(AuthFormEmailChanged(email: text));
                          },
                        ),
                        CustomPasswordTextFormField(
                          labelText: 'Sandi',
                          hintText: 'Masukan Sandi',
                          initialValue: loginForm.password,
                          suffixIcon: PasswordSuffixIcon(
                            obscurePassword: loginForm.obsecurePassword,
                            onTap: () => context.read<AuthBloc>().add(
                                  AuthFormObsecurePasswordChanged(
                                    obsecure: !loginForm.obsecurePassword,
                                  ),
                                ),
                          ),
                          validator: validatePasswordLogin,
                          obscureText: loginForm.obsecurePassword,
                          onChanged: (text) => context
                              .read<AuthBloc>()
                              .add(AuthFormPasswordChanged(password: text)),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            CustomCheckbox(
                              isChecked: loginForm.isRememberMe,
                              labelText: Text(
                                'Ingat Saya',
                                style: bSubtitle3.copyWith(
                                  color: colorScheme.onTertiaryContainer,
                                ),
                              ),
                              onChanged: (value) {
                                context.read<AuthBloc>().add(
                                      AuthFormIsRememberMe(
                                        isRememberMe: value!,
                                      ),
                                    );
                              },
                            ),
                            CustomTextButton(
                              onTap: () {
                                Navigator.pushNamed(
                                    context, '/forgot-password');
                              },
                              child: Text(
                                'Lupa Sandi?',
                                style: bSubtitle3.copyWith(
                                  color: colorScheme.onTertiaryContainer,
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 16),
                        CustomPrimaryButton(
                          width: MediaQuery.of(context).size.width,
                          onTap: () {
                            if (formKey.currentState?.validate() == true) {
                              context
                                  .read<AuthBloc>()
                                  .add(const OnAuthLoginUser());
                            }
                          },
                          child: (loginForm.isLoading == true)
                              ? SizedBox(
                                  width: 20,
                                  height: 20,
                                  child: CircularProgressIndicator(
                                    color: colorScheme.onPrimary,
                                  ),
                                )
                              : Text(
                                  'Masuk',
                                  style: bButton1.copyWith(
                                      color: colorScheme.onPrimary),
                                ),
                        ),
                        const SizedBox(height: 33),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Belum punya akun?',
                              style: bSubtitle4.copyWith(
                                  color: colorScheme.tertiary),
                            ),
                            CustomTextButton(
                              onTap: () {
                                Navigator.pushNamed(context, '/register');
                              },
                              child: Text(
                                'Daftar',
                                style: bSubtitle3.copyWith(
                                    color: colorScheme.primary),
                              ),
                            ),
                          ],
                        ),
                        CustomTextButton(
                          onTap: () {
                            Navigator.pushNamed(context, '/home');
                          },
                          child: Text(
                            'Home',
                            style:
                                bSubtitle3.copyWith(color: colorScheme.primary),
                          ),
                        ),
                      ]),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
