import 'package:flutter/material.dart';
import 'package:frontmobile_blog_app/presentation/widget/show_alert_message.dart';

class DataFiledScreen extends StatelessWidget {
  const DataFiledScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: showAlertMessage(
            context,
            'assets/images/filed.png',
            'Data Gagal Terkirim',
            'Whoops...Sepertinya data anda gagal terkirim, silahkan kirim ulang',
            'Coba Lagi',
          ),
        ),
      ),
    );
  }
}
