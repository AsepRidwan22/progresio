import 'package:flutter/material.dart';
import 'package:frontmobile_blog_app/presentation/widget/show_alert_message.dart';

class DataSuccessScreen extends StatelessWidget {
  const DataSuccessScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: showAlertMessage(
            context,
            'assets/images/success.png',
            'Data Sukses Terkirim',
            'Yeay, data anda berhasil terkirim cek selengkapnya di form laporan',
            'Okay',
          ),
        ),
      ),
    );
  }
}
