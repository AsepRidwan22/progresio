import 'package:flutter/material.dart';
import 'package:frontmobile_blog_app/presentation/widget/show_alert_message.dart';

class NoConnectionScreen extends StatelessWidget {
  const NoConnectionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: showAlertMessage(
            context,
            'assets/images/noConnection.png',
            'Tidak ada koneksi Internet',
            'Whoops...Mohon periksa kembali koneksi internet Anda dan coba lagi',
            'Coba Lagi',
          ),
        ),
      ),
    );
  }
}
