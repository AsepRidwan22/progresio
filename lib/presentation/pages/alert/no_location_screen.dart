import 'package:flutter/material.dart';
import 'package:frontmobile_blog_app/presentation/widget/show_alert_message.dart';

class NoLocationScreen extends StatelessWidget {
  const NoLocationScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: showAlertMessage(
            context,
            'assets/images/noGps.png',
            'GPS Tidak Aktif',
            'Whoops...Sepertinya GPS Anda tidak aktif, ke pengaturan untuk mengaktifkan',
            'Pergi ke Pengaturan',
          ),
        ),
      ),
    );
  }
}
