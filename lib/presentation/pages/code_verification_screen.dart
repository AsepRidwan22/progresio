import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_app_bar.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_head_sub_text.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_primary_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_verification_text_field.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class CodeVerificationScreen extends StatelessWidget {
  CodeVerificationScreen({super.key})
      : focus1 = FocusNode(),
        focus2 = FocusNode(),
        focus3 = FocusNode(),
        focus4 = FocusNode(),
        formKey = GlobalKey<FormState>();

  final FocusNode focus1;
  final FocusNode focus2;
  final FocusNode focus3;
  final FocusNode focus4;
  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Scaffold(
      body: SafeArea(
        child: CustomScrollView(
          slivers: <Widget>[
            SliverAppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: CustomAppBar(
                onTap: () {},
              ),
              surfaceTintColor: Colors.white,
              pinned: true,
            ),
            SliverPadding(
              padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: MediaQuery.of(context).size.height * 0.08,
              ),
              sliver: SliverList(
                delegate: SliverChildListDelegate(
                  [
                    SvgPicture.asset(
                      "assets/images/codeVerification.svg",
                      height: 200.0,
                    ),
                    const SizedBox(
                      height: 40,
                    ),
                    const CustomHeadSubText(
                      mainText: 'Kode Verifikasi',
                      subText:
                          'Silahkan input token untuk bergabung dengan workspace',
                    ),
                    Form(
                      key: formKey,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          CustomVerificationTextField(
                            focusNow: focus1,
                            focusNext: focus2,
                            field: 1,
                          ),
                          CustomVerificationTextField(
                            focusNow: focus2,
                            focusNext: focus3,
                            field: 2,
                          ),
                          CustomVerificationTextField(
                            focusNow: focus3,
                            focusNext: focus4,
                            field: 3,
                          ),
                          CustomVerificationTextField(
                            focusNow: focus4,
                            focusNext: focus4,
                            field: 4,
                          ),
                        ],
                      ),
                    ),
                    CustomPrimaryButton(
                      width: MediaQuery.of(context).size.width,
                      onTap: () {
                        // if (formKey.currentState?.validate() == true) {
                        //   // context.read<AuthBloc>().add(const OnAuthLoginUser());
                        // }
                        Navigator.pushNamed(context, '/set-new-password');
                      },
                      child: (false == true)
                          ? SizedBox(
                              width: 20,
                              height: 20,
                              child: CircularProgressIndicator(
                                color: colorScheme.onPrimary,
                              ),
                            )
                          : Text(
                              'Lanjut',
                              style: bButton1.copyWith(
                                color: colorScheme.onPrimary,
                              ),
                            ),
                    ),
                    const SizedBox(
                      height: 100,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Sudah punya akun?',
                          style:
                              bSubtitle4.copyWith(color: colorScheme.tertiary),
                        ),
                        CustomTextButton(
                          onTap: () {
                            Navigator.pushNamed(context, '/login');
                          },
                          child: Text(
                            'Masuk',
                            style:
                                bSubtitle3.copyWith(color: colorScheme.primary),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
