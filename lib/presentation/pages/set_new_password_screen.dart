import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontmobile_blog_app/common/regex_validation_field.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_app_bar.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_head_sub_text.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_password_text_form_field.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_primary_button.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class SetNewPasswordScreen extends StatefulWidget {
  const SetNewPasswordScreen({super.key});

  @override
  State<SetNewPasswordScreen> createState() => _SetNewPasswordScreenState();
}

class _SetNewPasswordScreenState extends State<SetNewPasswordScreen> {
  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    final formKey = GlobalKey<FormState>();
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: CustomAppBar(
                onTap: () {},
              ),
              surfaceTintColor: colorScheme.onPrimary,
              pinned: true,
            ),
            SliverPadding(
              padding: EdgeInsets.symmetric(
                horizontal: 20,
                vertical: MediaQuery.of(context).size.height * 0.08,
              ),
              sliver: SliverList(
                delegate: SliverChildListDelegate(
                  [
                    SvgPicture.asset(
                      "assets/images/forgotPassword.svg",
                      height: 150.0,
                    ),
                    const SizedBox(height: 32),
                    const CustomHeadSubText(
                      mainText: 'Atur Ulang Sandi',
                      subText:
                          'Sandi baru kamu harus berbeda dari sandi sebelumnya yang anda gunakan',
                    ),
                    CustomPasswordTextFormField(
                      labelText: 'Sandi',
                      isRequired: true,
                      hintText: 'Masukan Sandi',
                      initialValue: '',
                      suffixIcon: PasswordSuffixIcon(
                        obscurePassword: true,
                        onTap: () {},
                      ),
                      validator: validatePassword,
                      obscureText: true,
                      onChanged: (p0) {},
                    ),
                    CustomPasswordTextFormField(
                      labelText: 'Ulang Sandi',
                      isRequired: true,
                      hintText: 'Masukan Ulang Sandi',
                      initialValue: '',
                      suffixIcon: PasswordSuffixIcon(
                        obscurePassword: true,
                        onTap: () {},
                      ),
                      validator: validatePassword,
                      obscureText: true,
                      onChanged: (p0) {},
                    ),
                    CustomPrimaryButton(
                      width: MediaQuery.of(context).size.width,
                      onTap: () {
                        if (formKey.currentState?.validate() == true) {
                          // context.read<AuthBloc>().add(const OnAuthLoginUser());
                        }
                      },
                      child: (false == true)
                          ? SizedBox(
                              width: 20,
                              height: 20,
                              child: CircularProgressIndicator(
                                color: colorScheme.onPrimary,
                              ),
                            )
                          : Text(
                              'Masuk',
                              style: bButton1.copyWith(
                                color: colorScheme.onPrimary,
                              ),
                            ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
