// search_filter_bottom_sheet.dart
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_checkbox.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_primary_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_suffix_icon_form_field.dart';

Widget searchFilterBottomSheet(BuildContext context) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;
  return Container(
    decoration: BoxDecoration(
      color: colorScheme.onPrimary,
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(50.0),
        topRight: Radius.circular(50.0),
      ),
    ),
    width: MediaQuery.of(context).size.width,
    height: 620,
    padding: const EdgeInsets.all(16),
    child: Column(
      // crossAxisAlignment:
      //     CrossAxisAlignment.start,
      children: [
        Container(
          width: 100,
          height: 5.0,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: colorScheme.onTertiary,
            borderRadius: BorderRadius.circular(
              10.0,
            ), // Sesuaikan nilai sesuai kebutuhan
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Container(
          decoration: BoxDecoration(
            color: colorScheme.onSecondaryContainer,
            borderRadius: BorderRadius.circular(10),
          ),
          padding: const EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 12,
          ),
          child: Row(
            children: [
              Container(
                padding: const EdgeInsets.all(10),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: colorScheme.secondaryContainer,
                ),
                child: SvgPicture.asset(
                  "assets/icon/filterBlack.svg",
                  height: 12.0,
                ),
              ),
              const SizedBox(
                width: 12,
              ),
              Expanded(
                child: Text(
                  'Isi informasi basik dibawah ini, agar bisa mencari data yang akurat',
                  style: bSubtitle2.copyWith(
                    color: colorScheme.scrim,
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.only(
            top: 16,
            bottom: 12,
          ),
          child: Text(
            'Hasil Pencarian',
            style: bSubtitle1.copyWith(color: colorScheme.tertiary),
          ),
        ),
        Container(
          alignment: Alignment.centerLeft,
          child: Wrap(
            alignment: WrapAlignment.start,
            spacing: 12,
            runSpacing: 12,
            children: [
              _filterValue(
                context,
                value: 'Elektabilitas Paslon Kaltim',
              ),
              _filterValue(
                context,
                value: 'Laporan Covid',
              ),
              _filterValue(
                context,
                value: 'Laporan Kegiatan Marketing',
              ),
            ],
          ),
        ),
        Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.only(
            top: 16,
            bottom: 12,
          ),
          child: Text(
            'Tanggal',
            style: bSubtitle1.copyWith(color: colorScheme.tertiary),
          ),
        ),
        Container(
          alignment: Alignment.centerLeft,
          child: Wrap(
            alignment: WrapAlignment.start,
            spacing: 12,
            runSpacing: 12,
            children: [
              _filterValue(
                context,
                value: 'Hari Ini',
              ),
              _filterValue(
                context,
                value: 'Minggu Ini',
              ),
              _filterValue(
                context,
                value: 'Bulan Ini',
              ),
              _filterValue(
                context,
                value: 'Tahun Ini',
              ),
            ],
          ),
        ),
        const SizedBox(
          height: 16,
        ),
        Row(
          children: [
            Expanded(
              child: CustomTextSuffixIconFormField(
                labelText: 'Tanggal',
                isRequired: true,
                onChanged: (newValue) {},
              ),
            ),
            const SizedBox(
              width: 7,
            ),
            Expanded(
              child: CustomTextSuffixIconFormField(
                labelText: 'Sampai',
                isRequired: true,
                onChanged: (newValue) {},
              ),
            ),
          ],
        ),
        Container(
          alignment: Alignment.centerLeft,
          padding: const EdgeInsets.only(
            // top: 16,
            bottom: 12,
          ),
          child: Text(
            'Kepemilikan',
            style: bSubtitle1.copyWith(color: colorScheme.tertiary),
          ),
        ),
        CustomCheckbox(
          labelText: Text(
            'Laporan Saya',
            style: bSubtitle3.copyWith(color: colorScheme.onTertiaryContainer),
          ),
          isChecked: true,
          onChanged: (value) {},
        ),
        const SizedBox(
          height: 20,
        ),
        CustomPrimaryButton(
          width: MediaQuery.of(context).size.width,
          onTap: () {
            // if (formKey.currentState?.validate() == true) {
            //   context
            //       .read<AuthBloc>()
            //       .add(const OnAuthLoginUser());
            // }
          },
          child: (false == true)
              ? SizedBox(
                  width: 20,
                  height: 20,
                  child: CircularProgressIndicator(
                    color: colorScheme.onPrimary,
                  ),
                )
              : Text(
                  'Tampilkan',
                  style: bButton1.copyWith(
                    color: colorScheme.onPrimary,
                  ),
                ),
        ),
      ],
    ),
  );
}

Widget _filterValue(BuildContext context,
    {String? firstDate, String? lastDate, String? value}) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;

  return Container(
    // margin: const EdgeInsets.only(left: 6),
    padding: const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(6),
      border: Border.all(
        color: colorScheme.primary,
        width: 1,
      ),
      color: colorScheme.secondaryContainer,
    ),
    child: Text(
      (firstDate != null && lastDate != null)
          ? '$firstDate - $lastDate'
          : value!,
      style: bSubtitle2.copyWith(color: colorScheme.primary),
    ),
  );
}
