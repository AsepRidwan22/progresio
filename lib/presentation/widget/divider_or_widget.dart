import 'package:flutter/material.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

Widget dividOrWidget() {
  return Column(
    children: [
      Container(
        margin: const EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Expanded(
              child: Container(
                height: 2.0,
                color: outlineGrey,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0),
              child: Text(
                'Atau',
                style: bSubtitle2.copyWith(color: outlineGrey),
              ),
            ),
            Expanded(
              child: Container(
                height: 2.0,
                color: outlineGrey,
              ),
            ),
          ],
        ),
      ),
      const SizedBox(height: 16),
    ],
  );
}
