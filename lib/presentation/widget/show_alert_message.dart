import 'package:flutter/material.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_primary_button.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

Widget showAlertMessage(BuildContext context, String img, String title,
    String subTitle, String labelBtn) {
  ColorScheme colorScheme = Theme.of(context).colorScheme;
  return Column(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Column(
          children: [
            Image.asset(img),
            Text(
              title,
              style: bHeading2.copyWith(
                color: colorScheme.tertiary,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Text(
              subTitle,
              style: bSubtitle2.copyWith(
                color: colorScheme.onTertiaryContainer,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
      const SizedBox(
        height: 30,
      ),
      CustomPrimaryButton(
        width: MediaQuery.of(context).size.width,
        onTap: () {
          Navigator.pop(context);
        },
        child: Text(
          labelBtn,
          style: bButton1.copyWith(color: colorScheme.onPrimary),
        ),
      ),
    ],
  );
}
