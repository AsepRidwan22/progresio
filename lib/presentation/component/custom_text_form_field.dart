import 'package:flutter/material.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class CustomTextFormField extends StatelessWidget {
  final String labelText;
  final String? hintText;
  final String? initialValue;
  final String? Function(String?)? validator;
  final void Function(String) onChanged;
  final bool? isRequired;
  final TextInputType? textInputType;

  const CustomTextFormField(
      {super.key,
      required this.labelText,
      this.hintText,
      this.initialValue,
      this.validator,
      required this.onChanged,
      this.isRequired,
      this.textInputType});

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              labelText,
              style: bSubtitle1.copyWith(
                color: grey,
              ),
            ),
            isRequired == true
                ? Text(
                    '*',
                    style: bSubtitle1.copyWith(
                      color: colorScheme.error,
                    ),
                  )
                : const SizedBox(),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        TextFormField(
          initialValue: initialValue,
          style: bSubtitle1.copyWith(color: colorScheme.tertiary),
          decoration: InputDecoration(
            hintText: hintText,
          ),
          keyboardType: textInputType ?? TextInputType.text,
          validator: validator,
          onChanged: onChanged,
        ),
        const SizedBox(
          height: 16,
        ),
      ],
    );
  }
}
