import 'package:flutter/material.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class CustomTextFormFieldSearch extends StatelessWidget {
  final String hintText;
  final String? initialValue;
  final void Function(String) onChanged;
  final bool? isRequired;

  const CustomTextFormFieldSearch({
    super.key,
    required this.hintText,
    this.initialValue,
    required this.onChanged,
    this.isRequired,
  });

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        TextFormField(
          initialValue: initialValue,
          autofocus: true,
          style: bSubtitle1.copyWith(color: colorScheme.tertiary),
          decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(12),
            hintText: hintText,
            prefixIcon: Icon(
              Icons.search,
              color: colorScheme.scrim,
              size: 20,
            ),
          ),
          onChanged: onChanged,
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
