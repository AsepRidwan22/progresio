import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class CustomPrimaryIconTextButton extends StatelessWidget {
  final double width;
  final String text;
  final String icon;
  final Function() onTap;

  const CustomPrimaryIconTextButton({
    super.key,
    required this.width,
    required this.text,
    required this.icon,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;

    return Column(
      children: [
        OutlinedButton(
          onPressed: onTap,
          style: OutlinedButton.styleFrom(
            minimumSize: const Size(500.0, 50),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(
                icon,
                height: 19.0,
              ),
              const SizedBox(
                width: 5,
              ),
              Text(
                text,
                style:
                    bButton1.copyWith(color: colorScheme.onTertiaryContainer),
              ),
            ],
          ),
        ),
        const SizedBox(height: 16),
      ],
    );
  }
}
