import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_form_field_search.dart';
// import 'package:frontmobile_blog_app/theme/theme_data.dart';

class CustomSearchFilterAppBar extends StatelessWidget {
  final Function() onTap;

  const CustomSearchFilterAppBar({
    super.key,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    // ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 50, right: 10, left: 20),
          child: Stack(
            children: [
              Positioned.fill(
                bottom: 400,
                child: OverflowBox(
                  minWidth: 0.0,
                  maxWidth: double.infinity,
                  minHeight: 0.0,
                  maxHeight: double.infinity,
                  child: SvgPicture.asset(
                    "assets/backgrounds/textureBg.svg",
                    width: 620,
                  ),
                ),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: CustomTextFormFieldSearch(
                      hintText: 'Search',
                      initialValue: '',
                      onChanged: (text) {
                        // context.read<AuthBloc>().add(AuthFormEmailChanged(email: text));
                      },
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(bottom: 20),
                    child: CustomTextButton(
                      child: SvgPicture.asset(
                        "assets/icon/bell.svg",
                        height: 30.0,
                      ),
                      onTap: () {},
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
