import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class CustomPasswordTextFormField extends StatelessWidget {
  final String labelText;
  final String hintText;
  final String? initialValue;
  final Widget suffixIcon;
  final String? Function(String?)? validator;
  final bool obscureText;
  final void Function(String) onChanged;
  final bool? isRequired;

  const CustomPasswordTextFormField({
    super.key,
    required this.labelText,
    required this.hintText,
    required this.suffixIcon,
    this.initialValue,
    this.validator,
    required this.obscureText,
    required this.onChanged,
    this.isRequired,
  });

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              labelText,
              style: bSubtitle1.copyWith(
                color: grey,
              ),
            ),
            if (isRequired == true)
              Text(
                '*',
                style: bSubtitle1.copyWith(
                  color: colorScheme.error,
                ),
              ),
          ],
        ),
        const SizedBox(
          height: 5,
        ),
        TextFormField(
          initialValue: initialValue,
          style: bSubtitle1.copyWith(color: colorScheme.tertiary),
          decoration: InputDecoration(
            hintText: hintText,
            errorStyle: const TextStyle(
              color: Colors.red,
              fontSize: 12.0,
            ),
            suffixIcon: Padding(
              padding: const EdgeInsets.only(left: 5.0, right: 20.0),
              child: suffixIcon,
            ),
          ),
          validator: validator,
          obscureText: obscureText,
          onChanged: onChanged,
        ),
        const SizedBox(
          height: 16,
        ),
      ],
    );
  }
}

class PasswordSuffixIcon extends StatelessWidget {
  final bool obscurePassword;
  final void Function() onTap;

  const PasswordSuffixIcon({
    super.key,
    required this.obscurePassword,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: SvgPicture.asset(
        obscurePassword
            ? "assets/icon/eye-slash.svg"
            : "assets/icon/eyeSlash.svg",
      ),
    );
  }
}
