import 'package:flutter/material.dart';

class CustomPrimaryButton extends StatelessWidget {
  final double width;
  final Widget child;
  final Function() onTap;

  const CustomPrimaryButton({
    super.key,
    required this.width,
    required this.child,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ElevatedButton(
          onPressed: onTap,
          style: ElevatedButton.styleFrom(
            minimumSize: const Size(0, 0),
            maximumSize: const Size(double.infinity, 50.0),
          ),
          child: SizedBox(
            width: width,
            child: Center(child: child),
          ),
        ),
        const SizedBox(
          height: 16,
        )
      ],
    );
  }
}
