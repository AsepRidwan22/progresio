import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_secondary_icon_text_button.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_text_button.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class CustomHomeAppBar extends StatelessWidget {
  final Function() onTap;
  const CustomHomeAppBar({
    super.key,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 40,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CustomSecondaryIconTextButton(
                    onTap: () {
                      Navigator.pushNamed(context, '/search-filter');
                    },
                    buttonColor: colorScheme.onPrimary.withOpacity(0.3),
                    borderRadius: 6,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SvgPicture.asset(
                          "assets/icon/filter.svg",
                          height: 12.0,
                        ),
                        const SizedBox(
                          width: 4,
                        ),
                        Text(
                          'Filter',
                          style:
                              bSubtitle4.copyWith(color: colorScheme.onPrimary),
                        ),
                      ],
                    ),
                  ),
                  Row(
                    children: [
                      CustomTextButton(
                        child: SvgPicture.asset(
                          "assets/icon/bell.svg",
                          height: 30.0,
                        ),
                        onTap: () {},
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      CustomSecondaryIconTextButton(
                        onTap: onTap,
                        buttonColor: colorScheme.onPrimary.withOpacity(0.3),
                        borderRadius: 6,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "assets/icon/profile.svg",
                              height: 24.0,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              // const SizedBox(
              //   height: 14,
              // ),
            ],
          ),
        ),
      ],
    );
  }
}
