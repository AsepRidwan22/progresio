import 'package:flutter/material.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class CustomHeadSubText extends StatelessWidget {
  final String mainText;
  final String subText;

  const CustomHeadSubText({
    super.key,
    required this.mainText,
    required this.subText,
  });

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Column(
      children: [
        Align(
          alignment: Alignment.center,
          child: Text(
            mainText,
            style: bHeading1.copyWith(color: colorScheme.tertiary),
          ),
        ),
        const SizedBox(height: 8),
        Container(
          width: 275,
          alignment: Alignment.center,
          child: Text(
            subText,
            textAlign: TextAlign.center,
            style: bSubtitle2.copyWith(
              color: colorScheme.tertiary,
            ),
          ),
        ),
        const SizedBox(height: 25),
      ],
    );
  }
}
