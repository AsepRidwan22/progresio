import 'package:flutter/material.dart';
// import 'package:frontmobile_blog_app/theme/theme_data.dart';

class CustomCheckbox extends StatefulWidget {
  final bool isChecked;
  final Function(bool?) onChanged;
  final Widget labelText;

  const CustomCheckbox({
    super.key,
    required this.labelText,
    required this.isChecked,
    required this.onChanged,
  });
  @override
  State<CustomCheckbox> createState() => _CustomCheckboxState();
}

class _CustomCheckboxState extends State<CustomCheckbox> {
  @override
  Widget build(BuildContext context) {
    // ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        InkWell(
          onTap: () {
            widget.onChanged(!widget.isChecked);
          },
          child: Container(
            width: 20, // Sesuaikan dengan ukuran yang diinginkan
            height: 20,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey, // Warna border
                width: 1.0, // Lebar border
              ),
              borderRadius:
                  BorderRadius.circular(4.0), // Sesuaikan dengan kebutuhan
              color: widget.isChecked ? Colors.blue : Colors.transparent,
            ),
            child: widget.isChecked
                ? const Icon(
                    Icons.check,
                    size: 16,
                    color: Colors.white,
                  )
                : null,
          ),
        ),
        const SizedBox(width: 8),
        widget.labelText,
      ],
    );
  }
}
