import 'package:flutter/material.dart';

class CustomSecondaryIconTextButton extends StatelessWidget {
  final Widget child;
  final Function() onTap;
  final Color? buttonColor;
  final double? borderRadius;
  final Color? borderColor;
  final double? borderWidth;

  const CustomSecondaryIconTextButton({
    super.key,
    required this.child,
    required this.onTap,
    this.buttonColor,
    this.borderRadius,
    this.borderColor,
    this.borderWidth,
  });

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          color: buttonColor ?? colorScheme.secondaryContainer,
          borderRadius: BorderRadius.circular(borderRadius ?? 100),
          border: Border.all(
            color: borderColor ?? Colors.transparent,
            width: borderWidth ?? 0.0,
          ),
        ),
        padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 4),
        child: Center(child: child),
      ),
    );
  }
}
