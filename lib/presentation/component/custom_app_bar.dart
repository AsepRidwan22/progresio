import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:frontmobile_blog_app/presentation/component/custom_secondary_icon_text_button.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

class CustomAppBar extends StatelessWidget {
  final Function() onTap;
  const CustomAppBar({
    super.key,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    ColorScheme colorScheme = Theme.of(context).colorScheme;
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              decoration: BoxDecoration(
                color: Theme.of(context).colorScheme.primaryContainer,
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Center(
                child: SvgPicture.asset(
                  "assets/icon/back.svg",
                  height: 20.0,
                ),
              ),
            ),
          ),
          CustomSecondaryIconTextButton(
            onTap: onTap,
            child: Row(
              children: [
                SvgPicture.asset(
                  "assets/icon/questionMark.svg",
                  height: 14.0,
                ),
                const SizedBox(
                  width: 4,
                ),
                Text(
                  'Bantuan',
                  style: bSubtitle4.copyWith(color: colorScheme.primary),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
