part of 'dashboard_bloc.dart';

sealed class DashboardEvent extends Equatable {
  const DashboardEvent();

  @override
  List<Object> get props => [];
}

class IndexBottomNavChange extends DashboardEvent {
  final int newIndex;

  const IndexBottomNavChange({required this.newIndex});
}
