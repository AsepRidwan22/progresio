part of 'auth_bloc.dart';

class AuthState extends Equatable {
  final String? name;
  final String? email;
  final String? password;
  final String? passwordConf;
  final String message;
  final bool isLoading;
  final bool isRememberMe;
  final bool isTerms;
  final FormStatusEnum formStatus;
  final bool obsecurePassword;
  final bool obsecurePasswordRegister;
  final bool obsecurePasswordConf;
  final RequestStateEnum requestState;
  final UserEntity currentUser;
  final AuthStateEnum authStateEnum;
  final String? emailRegister;
  final String? passwordRegister;
  final String? nik;
  final String? noHp;

  const AuthState({
    this.name,
    this.email,
    this.password,
    this.passwordConf,
    required this.message,
    required this.isLoading,
    required this.isRememberMe,
    required this.isTerms,
    required this.formStatus,
    required this.obsecurePassword,
    required this.obsecurePasswordRegister,
    required this.obsecurePasswordConf,
    required this.requestState,
    required this.currentUser,
    required this.authStateEnum,
    this.emailRegister,
    this.passwordRegister,
    this.nik,
    this.noHp,
  });

  AuthState copyWith({
    String? name,
    String? email,
    String? password,
    String? passwordConf,
    String? message,
    bool? isLoading,
    bool? isRememberMe,
    bool? isTerms,
    FormStatusEnum? formStatus,
    bool? obsecurePassword,
    bool? obsecurePasswordRegister,
    bool? obsecurePasswordConf,
    bool? isDokter,
    RequestStateEnum? requestState,
    UserEntity? currentUser,
    AuthStateEnum? authStateEnum,
    String? emailRegister,
    String? passwordRegister,
    String? nik,
    String? noHp,
  }) {
    return AuthState(
      name: name ?? this.name,
      email: email ?? this.email,
      password: password ?? this.password,
      passwordConf: passwordConf ?? this.passwordConf,
      message: message ?? this.message,
      isLoading: isLoading ?? this.isLoading,
      isRememberMe: isRememberMe ?? this.isRememberMe,
      isTerms: isTerms ?? this.isTerms,
      formStatus: formStatus ?? this.formStatus,
      obsecurePassword: obsecurePassword ?? this.obsecurePassword,
      obsecurePasswordRegister:
          obsecurePasswordRegister ?? this.obsecurePasswordRegister,
      obsecurePasswordConf: obsecurePasswordConf ?? this.obsecurePasswordConf,
      requestState: requestState ?? this.requestState,
      currentUser: currentUser ?? this.currentUser,
      authStateEnum: authStateEnum ?? this.authStateEnum,
      emailRegister: emailRegister ?? this.emailRegister,
      passwordRegister: passwordRegister ?? this.passwordRegister,
      nik: nik ?? this.nik,
      noHp: noHp ?? this.noHp,
    );
  }

  @override
  List<Object?> get props => [
        name,
        email,
        password,
        passwordConf,
        message,
        isLoading,
        isRememberMe,
        isTerms,
        formStatus,
        obsecurePassword,
        obsecurePasswordRegister,
        obsecurePasswordConf,
        requestState,
        currentUser,
        authStateEnum,
        emailRegister,
        passwordRegister,
        nik,
        noHp,
      ];
}

class AuthInitial extends AuthState {
  const AuthInitial()
      : super(
          name: '',
          email: '',
          password: '',
          passwordConf: '',
          message: '',
          isLoading: false,
          isRememberMe: false,
          isTerms: false,
          formStatus: FormStatusEnum.initForm,
          obsecurePassword: true,
          obsecurePasswordRegister: true,
          obsecurePasswordConf: true,
          requestState: RequestStateEnum.initial,
          currentUser: const UserEntity(
            id: '',
            name: '',
            nik: '',
            noHp: '',
            email: '',
            role: '',
            createdAt: null,
            updatedAt: null,
          ),
          authStateEnum: AuthStateEnum.isInitial,
          emailRegister: '',
          passwordRegister: '',
          nik: '',
          noHp: '',
        );
}
