import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:frontmobile_blog_app/common/auth_state_enum.dart';
import 'package:frontmobile_blog_app/common/form_status_enum.dart';
import 'package:frontmobile_blog_app/common/request_state_enum.dart';
import 'package:frontmobile_blog_app/domain/entities/user_entity.dart';
import 'package:frontmobile_blog_app/domain/usecases/login_usecase.dart';
import 'package:frontmobile_blog_app/domain/usecases/logout_usecase.dart';
import 'package:frontmobile_blog_app/domain/usecases/register_usecase.dart';
part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final LoginUsecase loginUsecase;
  final RegisterUsecase registerUsecase;
  final LogoutUsecase logoutUsecase;
  AuthBloc(this.loginUsecase, this.registerUsecase, this.logoutUsecase)
      : super(const AuthInitial()) {
    on<AuthFormNameChanged>((event, emit) async {
      final newName = event.name;
      emit(state.copyWith(
        name: newName,
      ));
    });

    on<AuthFormEmailChanged>((event, emit) async {
      final newEmail = event.email;
      emit(state.copyWith(
        email: newEmail,
      ));
    });

    on<AuthFormEmailRegisterChanged>((event, emit) async {
      final newEmail = event.email;
      emit(state.copyWith(
        emailRegister: newEmail,
      ));
    });

    on<AuthFormPasswordChanged>((event, emit) async {
      final newPassword = event.password;
      emit(state.copyWith(
        password: newPassword,
      ));
    });

    on<AuthFormPasswordRegiserChanged>((event, emit) async {
      final newPassword = event.password;
      emit(state.copyWith(
        passwordRegister: newPassword,
      ));
    });

    on<AuthFormPasswordConfChanged>((event, emit) async {
      final newPassword = event.passwordConf;
      emit(state.copyWith(
        passwordConf: newPassword,
      ));
    });

    on<AuthFormNikChanged>((event, emit) async {
      final newNik = event.nik;
      emit(state.copyWith(
        nik: newNik,
      ));
    });

    on<AuthFormNoHpChanged>((event, emit) async {
      final newNoHp = event.noHp;
      emit(state.copyWith(
        noHp: newNoHp,
      ));
    });

    on<AuthFormIsLoading>((event, emit) {
      final newRememberMe = event.isLoading;

      emit(state.copyWith(
        isLoading: newRememberMe,
      ));
    });

    on<AuthFormIsRememberMe>((event, emit) {
      final isRememberMe = event.isRememberMe;

      emit(state.copyWith(
        isRememberMe: isRememberMe,
      ));
    });

    on<AuthFormIsTerms>((event, emit) {
      final isTerms = event.isTerms;

      emit(state.copyWith(
        isTerms: isTerms,
      ));
    });

    on<OnAuthLoginUser>((event, emit) async {
      emit(state.copyWith(
          formStatus: FormStatusEnum.submittingForm, isLoading: true));

      final result = await loginUsecase.execute(
        state.email!,
        state.password!,
      );

      result.fold(
        (failure) {
          debugPrint('login gagal bro');
          emit(state.copyWith(
            formStatus: FormStatusEnum.failedSubmission,
            message: failure.message,
            isLoading: false,
          ));
        },
        (data) {
          debugPrint('login berhasil bloc');
          emit(state.copyWith(
            formStatus: FormStatusEnum.successSubmission,
            currentUser: data.user,
            message: data.message,
            isLoading: false,
            email: '',
            password: '',
          ));
          debugPrint(data.user!.email);
        },
      );
    });

    on<OnAuthRegisterUser>((event, emit) async {
      emit(state.copyWith(
          formStatus: FormStatusEnum.submittingForm, isLoading: true));
      debugPrint('cek name ${state.name}');
      debugPrint('cek email ${state.emailRegister}');
      debugPrint('cek password ${state.passwordRegister}');
      debugPrint('cek nik ${state.nik}');
      debugPrint('cek noHp ${state.noHp}');

      final result = await registerUsecase.execute(
        state.name!,
        state.emailRegister!,
        state.passwordRegister!,
        state.nik!,
        state.noHp!,
      );

      result.fold(
        (failure) {
          emit(state.copyWith(
            formStatus: FormStatusEnum.failedSubmission,
            message: failure.message,
            isLoading: false,
          ));
        },
        (data) {
          emit(state.copyWith(
            formStatus: FormStatusEnum.successSubmission,
            message: data.message,
            isLoading: false,
            name: '',
            emailRegister: '',
            passwordRegister: '',
            nik: '',
            noHp: '',
          ));
        },
      );
    });

    on<OnAuthLogout>((event, emit) async {
      emit(state.copyWith(
        formStatus: FormStatusEnum.submittingForm,
        isLoading: true,
      ));

      final result = await logoutUsecase.execute();

      result.fold(
        (failure) {
          emit(state.copyWith(
            authStateEnum: AuthStateEnum.isFiled,
            message: failure.message,
            isLoading: false,
          ));
        },
        (data) {
          emit(const AuthInitial());
        },
      );
    });

    on<AuthFormObsecurePasswordChanged>((event, emit) {
      final newObsecure = event.obsecure;
      emit(state.copyWith(
        obsecurePassword: newObsecure,
      ));
    });

    on<AuthFormObsecurePasswordRegisterChanged>((event, emit) {
      final newObsecureRegister = event.obsecureRegister;
      emit(state.copyWith(
        obsecurePasswordRegister: newObsecureRegister,
      ));
    });

    on<AuthFormObsecureConfPasswordChanged>((event, emit) {
      final newObsecureConf = event.obsecure;
      emit(state.copyWith(
        obsecurePasswordConf: newObsecureConf,
      ));
    });
  }
}
