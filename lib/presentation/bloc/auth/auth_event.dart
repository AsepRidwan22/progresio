part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class AuthFormNameChanged extends AuthEvent {
  final String name;

  const AuthFormNameChanged({required this.name});

  @override
  List<Object> get props => [name];
}

class AuthFormEmailChanged extends AuthEvent {
  final String email;

  const AuthFormEmailChanged({required this.email});

  @override
  List<Object> get props => [email];
}

class AuthFormPasswordChanged extends AuthEvent {
  final String password;

  const AuthFormPasswordChanged({required this.password});

  @override
  List<Object> get props => [password];
}

class AuthFormEmailRegisterChanged extends AuthEvent {
  final String email;

  const AuthFormEmailRegisterChanged({required this.email});

  @override
  List<Object> get props => [email];
}

class AuthFormPasswordRegiserChanged extends AuthEvent {
  final String password;

  const AuthFormPasswordRegiserChanged({required this.password});

  @override
  List<Object> get props => [password];
}

class AuthFormNikChanged extends AuthEvent {
  final String nik;

  const AuthFormNikChanged({required this.nik});

  @override
  List<Object> get props => [nik];
}

class AuthFormNoHpChanged extends AuthEvent {
  final String noHp;

  const AuthFormNoHpChanged({required this.noHp});

  @override
  List<Object> get props => [noHp];
}

class AuthFormPasswordConfChanged extends AuthEvent {
  final String passwordConf;

  const AuthFormPasswordConfChanged({required this.passwordConf});

  @override
  List<Object> get props => [passwordConf];
}

class OnAuthLoginUser extends AuthEvent {
  const OnAuthLoginUser();
  @override
  List<Object> get props => [];
}

class OnAuthRegisterUser extends AuthEvent {
  const OnAuthRegisterUser();
  @override
  List<Object> get props => [];
}

class OnAuthLogout extends AuthEvent {
  const OnAuthLogout();
  @override
  List<Object> get props => [];
}

class AuthFormObsecurePasswordChanged extends AuthEvent {
  final bool obsecure;
  const AuthFormObsecurePasswordChanged({required this.obsecure});

  @override
  List<Object> get props => [obsecure];
}

class AuthFormObsecurePasswordRegisterChanged extends AuthEvent {
  final bool obsecureRegister;
  const AuthFormObsecurePasswordRegisterChanged(
      {required this.obsecureRegister});

  @override
  List<Object> get props => [obsecureRegister];
}

class AuthFormObsecureConfPasswordChanged extends AuthEvent {
  final bool obsecure;
  const AuthFormObsecureConfPasswordChanged({required this.obsecure});

  @override
  List<Object> get props => [obsecure];
}

class AuthFormIsLoading extends AuthEvent {
  final bool isLoading;
  const AuthFormIsLoading({required this.isLoading});

  @override
  List<Object> get props => [isLoading];
}

class AuthFormIsRememberMe extends AuthEvent {
  final bool isRememberMe;
  const AuthFormIsRememberMe({required this.isRememberMe});

  @override
  List<Object> get props => [isRememberMe];
}

class AuthFormIsTerms extends AuthEvent {
  final bool isTerms;
  const AuthFormIsTerms({required this.isTerms});

  @override
  List<Object> get props => [isTerms];
}

class ResetStateAuth extends AuthEvent {}
