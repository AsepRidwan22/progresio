import 'package:equatable/equatable.dart';

class RegisterModel extends Equatable {
  final bool success;
  final String message;

  const RegisterModel({
    required this.success,
    required this.message,
  });

  @override
  List<Object?> get props => [success, message];

  factory RegisterModel.fromJson(Map<String, dynamic> json) {
    return RegisterModel(
      success: json['success'] ?? false,
      message: json['message'] ?? '',
    );
  }
}
