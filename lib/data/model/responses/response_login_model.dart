import 'package:equatable/equatable.dart';
import 'package:frontmobile_blog_app/domain/entities/user_entity.dart';

class ResponseLoginModel extends Equatable {
  final bool success;
  final String message;
  final UserEntity? user;

  const ResponseLoginModel({
    required this.success,
    required this.message,
    this.user,
  });

  @override
  List<Object?> get props => [success, message, user];

  factory ResponseLoginModel.fromJson(Map<String, dynamic> json) {
    return ResponseLoginModel(
      success: json['success'] ?? false,
      message: json['message'] ?? 'Information not available.',
      user: json['user'] != null ? UserEntity.fromJson(json['user']) : null,
    );
  }
}
