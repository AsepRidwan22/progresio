import 'package:equatable/equatable.dart';

class RequestRegistrationModel extends Equatable {
  final String? name;
  final String? email;
  final String? password;
  final String? nik;
  final String? noHp;

  const RequestRegistrationModel({
    this.name,
    this.email,
    this.password,
    this.nik,
    this.noHp,
  });

  @override
  List<Object?> get props => [name, email, password];

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'email': email,
      'password': password,
      'nik': nik,
      'phone_number': noHp,
      'role': 'regular',
    };
  }

  factory RequestRegistrationModel.fromJson(Map<String, dynamic> json) {
    return RequestRegistrationModel(
      name: json['name'],
      email: json['email'],
      password: json['password'],
      nik: json['nik'],
      noHp: json['phone_number'],
      // role: json['role'],
    );
  }
}
