import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:frontmobile_blog_app/common/failure.dart';
import 'package:frontmobile_blog_app/data/datasources/user_remote_datasource_impl.dart';
import 'package:frontmobile_blog_app/data/model/requests/request_registration_model.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_login_model.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_register_model.dart';
import 'package:frontmobile_blog_app/domain/repositories/user_repository.dart';

class UserRepositoryImpl implements UserRepository {
  final UserRemoteDataSource remoteDataSource;

  UserRepositoryImpl({required this.remoteDataSource});

  @override
  Future<Either<Failure, ResponseLoginModel>> login(
      String email, String password) async {
    try {
      final result = await remoteDataSource.login(email, password);
      return Right(result);
    } on SocketException {
      return const Left(ConnectionFailure('Failed to Connect to the Network'));
    } on ServerFailure catch (serverFailure) {
      return Left(ServerFailure(serverFailure.message));
    }
  }

  @override
  Future<Either<Failure, String>> logout() async {
    try {
      final result = await remoteDataSource.logout();
      return Right(result);
    } on SocketException {
      return const Left(ConnectionFailure('Failed to Connect to the Network'));
    }
  }

  @override
  Future<Either<Failure, RegisterModel>> register(
      RequestRegistrationModel registrationModel) async {
    try {
      final result = await remoteDataSource.register(registrationModel);
      return Right(result);
    } on SocketException {
      return const Left(ConnectionFailure('Failed to Connect to the Network'));
    } on ServerFailure catch (serverFailure) {
      return Left(serverFailure);
    } catch (error) {
      return Left(ServerFailure('Unexpected Error: $error'));
    }
  }
}
