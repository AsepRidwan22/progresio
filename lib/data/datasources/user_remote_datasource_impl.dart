import 'package:flutter/material.dart';
import 'package:frontmobile_blog_app/common/failure.dart';
import 'package:frontmobile_blog_app/data/helper/secure_storage_helper.dart';
import 'package:frontmobile_blog_app/data/model/requests/request_registration_model.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_login_model.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_register_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:frontmobile_blog_app/common/config.dart';

abstract class UserRemoteDataSource {
  Future<ResponseLoginModel> login(String email, String password);
  Future<String> logout();
  Future<RegisterModel> register(RequestRegistrationModel registrationModel);
}

class UserRemoteDataSourceImpl implements UserRemoteDataSource {
  final baseURL = Config().baseURL;
  final http.Client client;
  final SecureStorageHelper secureStorageHelper;

  UserRemoteDataSourceImpl(
      {required this.client, required this.secureStorageHelper});

  @override
  Future<ResponseLoginModel> login(String email, String password) async {
    final response = await client.post(
      Uri.parse('$baseURL/api/login'),
      body: jsonEncode({
        'email': email,
        'password': password,
      }),
      headers: {'Content-Type': 'application/json'},
    );

    if (response.statusCode == 200) {
      final responseData = jsonDecode(response.body);
      final token = responseData['access_token'];
      await secureStorageHelper.saveToken(token);
      return ResponseLoginModel.fromJson(responseData);
    } else {
      debugPrint('datasoruce login filed');
      final errorResponse = jsonDecode(response.body);
      throw ServerFailure(errorResponse['message']);
    }
  }

  @override
  Future<RegisterModel> register(
      RequestRegistrationModel registrationModel) async {
    debugPrint('request json ${registrationModel.toJson()}');
    final response = await client.post(
      Uri.parse('$baseURL/api/register'),
      body: jsonEncode(registrationModel.toJson()),
      headers: {'Content-Type': 'application/json'},
    );
    debugPrint('register datasource jalan');
    if (response.statusCode == 200) {
      final responseData = jsonDecode(response.body);

      return RegisterModel.fromJson(responseData);
    } else {
      final errorResponse = jsonDecode(response.body);

      throw ServerFailure(errorResponse['message']);
    }
  }

  @override
  Future<String> logout() async {
    final String? token = await secureStorageHelper.getToken();

    if (token == null) {
      throw const ServerFailure('Token not available');
    }

    final response = await client.post(
      Uri.parse('$baseURL/api/logout'),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      final responseData = jsonDecode(response.body);

      await secureStorageHelper.deleteToken();

      return responseData['message'];
    } else {
      final errorResponse = jsonDecode(response.body);

      throw ServerFailure(errorResponse['message']);
    }
  }
}
