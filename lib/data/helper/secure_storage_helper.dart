import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class SecureStorageHelper {
  final FlutterSecureStorage _storage;

  SecureStorageHelper(this._storage);

  static const tokenKey = 'user_token';

  Future<void> saveToken(String token) async {
    await _storage.write(key: tokenKey, value: token);
  }

  Future<String?> getToken() async {
    return await _storage.read(key: tokenKey);
  }

  Future<void> updateToken(String newToken) async {
    await _storage.delete(key: tokenKey);
    await saveToken(newToken);
  }

  Future<void> deleteToken() async {
    await _storage.delete(key: tokenKey);
  }
}
