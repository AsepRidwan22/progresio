import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:frontmobile_blog_app/common/app_routes.dart';
import 'package:frontmobile_blog_app/injection.dart' as di;
import 'package:frontmobile_blog_app/presentation/bloc/auth/auth_bloc.dart';
import 'package:frontmobile_blog_app/presentation/bloc/auth/verification_form_bloc.dart';
import 'package:frontmobile_blog_app/presentation/pages/login_screen.dart';
import 'package:frontmobile_blog_app/theme/theme_data.dart';

void main() async {
  di.init();
  runApp(MultiBlocProvider(
    providers: [
      BlocProvider(
        create: (_) => di.locator<AuthBloc>(),
      ),
      BlocProvider(
        create: (_) => di.locator<VerificationFormBloc>(),
      ),
    ],
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: AppRoutes.routes,
      title: 'Flutter Demo',
      theme: lightTheme,
      home: const LoginScreen(),
    );
  }
}
