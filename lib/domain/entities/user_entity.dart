import 'package:equatable/equatable.dart';

class UserEntity extends Equatable {
  final String? id;
  final String name;
  final String email;
  final String role;
  final String nik;
  final String noHp;
  final String? password;
  final DateTime? createdAt;
  final DateTime? updatedAt;

  const UserEntity({
    this.id,
    required this.name,
    required this.email,
    required this.nik,
    required this.noHp,
    required this.role,
    this.password,
    this.createdAt,
    this.updatedAt,
  });

  factory UserEntity.fromJson(Map<String, dynamic> json) {
    return UserEntity(
      id: json['id'] as String?,
      name: json['name'] as String,
      email: json['email'] as String,
      role: json['role'] as String,
      nik: json['nik'] as String,
      noHp: json['phone_number'] as String,
      password: json['password'] as String?,
      createdAt: json['created_at'] != null
          ? DateTime.parse(json['created_at'])
          : null,
      updatedAt: json['updated_at'] != null
          ? DateTime.parse(json['updated_at'])
          : null,
    );
  }

  @override
  List<Object?> get props {
    return [
      id,
      name,
      email,
      role,
      nik,
      noHp,
      password,
      createdAt,
      updatedAt,
    ];
  }
}
