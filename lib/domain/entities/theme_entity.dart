import 'package:equatable/equatable.dart';

class ThemeEntity extends Equatable {
  final bool isDarkMode;

  const ThemeEntity(this.isDarkMode);

  @override
  List<Object> get props => [isDarkMode];
}
