import 'package:dartz/dartz.dart';
import 'package:frontmobile_blog_app/domain/entities/theme_entity.dart';
import 'package:frontmobile_blog_app/common/failure.dart';

abstract class ThemeRepository {
  Future<Either<Failure, ThemeEntity>> getTheme();
}
