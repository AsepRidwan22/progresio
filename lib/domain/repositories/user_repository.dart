import 'package:dartz/dartz.dart';
import 'package:frontmobile_blog_app/common/failure.dart';
import 'package:frontmobile_blog_app/data/model/requests/request_registration_model.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_login_model.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_register_model.dart';

abstract class UserRepository {
  Future<Either<Failure, RegisterModel>> register(
      RequestRegistrationModel registrationModel);
  Future<Either<Failure, ResponseLoginModel>> login(
    String email,
    String password,
  );
  Future<Either<Failure, String>> logout();
}
