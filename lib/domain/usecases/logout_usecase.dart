import 'package:frontmobile_blog_app/common/failure.dart';
import 'package:frontmobile_blog_app/domain/repositories/user_repository.dart';
import 'package:dartz/dartz.dart';

class LogoutUsecase {
  final UserRepository repository;

  LogoutUsecase(this.repository);

  Future<Either<Failure, String>> execute() async {
    return repository.logout();
  }
}
