import 'package:frontmobile_blog_app/common/failure.dart';
import 'package:frontmobile_blog_app/data/model/requests/request_registration_model.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_register_model.dart';
import 'package:frontmobile_blog_app/domain/repositories/user_repository.dart';
import 'package:dartz/dartz.dart';

class RegisterUsecase {
  final UserRepository repository;

  RegisterUsecase(this.repository);

  Future<Either<Failure, RegisterModel>> execute(
    String name,
    String email,
    String password,
    String nik,
    String noHp,
  ) async {
    return repository.register(RequestRegistrationModel(
      name: name,
      email: email,
      password: password,
      nik: nik,
      noHp: noHp,
    ));
  }
}
