import 'package:frontmobile_blog_app/common/failure.dart';
import 'package:frontmobile_blog_app/data/model/responses/response_login_model.dart';
import 'package:frontmobile_blog_app/domain/repositories/user_repository.dart';
import 'package:dartz/dartz.dart';

class LoginUsecase {
  final UserRepository repository;

  LoginUsecase(this.repository);

  Future<Either<Failure, ResponseLoginModel>> execute(
    String email,
    String password,
  ) async {
    return repository.login(email, password);
  }
}
