import 'package:flutter/material.dart';
import 'package:frontmobile_blog_app/presentation/pages/code_verification_screen.dart';
import 'package:frontmobile_blog_app/presentation/pages/forgot_password_screen.dart';
import 'package:frontmobile_blog_app/presentation/pages/home_screen.dart';
import 'package:frontmobile_blog_app/presentation/pages/login_screen.dart';
import 'package:frontmobile_blog_app/presentation/pages/alert/no_connection_screen.dart';
import 'package:frontmobile_blog_app/presentation/pages/profile_screen.dart';
import 'package:frontmobile_blog_app/presentation/pages/register_screen.dart';
import 'package:frontmobile_blog_app/presentation/pages/search_result_filter_screen.dart';
import 'package:frontmobile_blog_app/presentation/pages/set_new_password_screen.dart';

class AppRoutes {
  static const String home = '/home';
  static const String register = '/register';
  static const String login = '/login';
  static const String forgotPassword = '/forgot-password';
  static const String codeVerification = '/code-verification';
  static const String setNewPassword = '/set-new-password';
  static const String profile = '/profile';
  static const String listUser = '/list-user';
  static const String searchFilter = '/search-filter';
  static const String noConnection = '/no-connection';

  static Map<String, WidgetBuilder> routes = {
    home: (context) => const HomeScreen(),
    register: (context) => const RegisterScreen(),
    login: (context) => const LoginScreen(),
    forgotPassword: (context) => const ForgotPasswordScreen(),
    codeVerification: (context) => CodeVerificationScreen(),
    setNewPassword: (context) => const SetNewPasswordScreen(),
    searchFilter: (context) => const SearchResultFilterScreen(),
    profile: (context) => const ProfileScreen(),
    noConnection: (context) => const NoConnectionScreen(),
  };
}
